//
//  APIs.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

//
import Foundation
import LanguageManager_iOS
class APIs {
    static let Instance = APIs()
    private init() {}
    
    private let url = "http://service.madariskm.com"
    
    public func getHeader() -> [String: String]{
        var header = [
            "Accept" : "application/json" , "Authorization" : "Bearer \(userData.Instance.token ?? "")" ,
            "Accept-Language":"ar" , "Content-Type":"application/x-www-form-urlencoded"
        ]
        if LanguageManager.shared.currentLanguage == .en {
            header["Accept-Language"] = "en"
            return header
        }
        if LanguageManager.shared.currentLanguage == .ku {
            header["Accept-Language"] = "ku"
            return header
        }
        return header
    }
    public func getRooms()-> String{
      return url + "/api/chat/rooms"
    }
    //    \(userData.Instance.token ?? "")
    public func registeration() -> String{
        return url + "/api/auth/register"
    }
    public func resetNotificationBadge() -> String{
        return url + "notifications?read_all=1"
    }

    public func resetPassword() -> String{
        return url + "/api/auth/password/reset"
    }
    public func login() -> String{
        return url + "/api/auth/login"
    }
    public func UserShowProfile() -> String{
        return url + "/api/me"
    }
    public func resetPasswordVerifiy() -> String{
        return url + "/api/auth/password/forget"
    }
    public func resetPasswordVerifiy2() -> String{
        return url + "/api/auth/password/check-code"
    }
    public func UserUpdateprofile() -> String{
        return url + "/api/me/update"
    }
    public func ListStudentGradeSubjects() -> String{
        return url + "/api/subjects"
    }
    public func ListSchoolNews() -> String{
        return url + "/api/news"
    }
    public func Attendance() -> String{
        return url + "/api/attendance"
    }
    public func AttendanceInSpecificDay(date:String) -> String{
        return url + "/api/searchAttendance?date=\(date)"
    }
    public func ExamsResults() -> String{
        return url + "/api/results"
    }
    //    public func showSpecificNews() -> String{
    //        return url + "/news/{news}"
    //    }
    public func ListSchoolAlbums()-> String{
        return url + "/api/school_media"
    }
    public func directorWord()-> String{
        return url + "/api/admin_word"
    }
    public func scheduleForDay(day:Int)-> String{
        return url + "/api/schedules?day=\(day)"
    }
    public func showCurrentStudent(id:Int)-> String{
        return url + "/api/students/\(id)"
    }
    public func addLesson()-> String{
        return url + "/api/lessons"
    }
    public func showAllAvilableExams()-> String{
        return url + "/api/exams"
    }
    public func showCurrentExams(id:Int)-> String{
        return url + "/api/exams/\(id)"
    }
    public func sendMessage(id:String)-> String{
        return url + "/api/chat/rooms/\(id)/send-message"
    }
    public func getTeachers()-> String{
        return url + "/api/teachers"
    }
    public func getFlags()-> String{
        return url + "/api/notifications/flags/get"
    }
    public func getSubjects()-> String{
        return url + "/api/subjects"
    }
    public func getLessons(id:Int,subjectId:Int)-> String{
           return url + "/api/lessons?section_id=\(id)&subject_id=\(subjectId)"
       }
    
    public func getSchoolNews()-> String{
        return url + "/api/news"
    }
    public func getSchoolNew(id:String)-> String{
           return url + "/api/news/\(id)"
       }
    public func getCurrentSchoolNews(id:Int)-> String{
        return url + "/api/news/\(id)"
    }
    public func showChat(id:Int)-> String{
        return url + "/api/chat/users/\(id)"
    }
    public func showChat()-> String{
           return url + "/api/chat/users"
       }
    public func showChatByRoom(id:Int)-> String{
          return url + "/api/chat/rooms/\(id)"
      }
    public func showChatWithAdmin()-> String{
        return url + "/api/chat/adminstration-room"
    }
    public func getSchoolMedia()-> String{
        return url + "/api/school_media"
    }
    public func getSchoolMedia(id:Int)-> String{
           return url + "/api/school_media/\(id)"
       }
    public func getSchoolInstallments()-> String{
        return url + "/api/installments"
    }
    public func getAllStudent()-> String{
        return url + "/api/students"
    }
    public func getStudentWithoutRoom()-> String{
        return url + "/api/students?only_does_not_have_chat=1"
    }
    public func getRoomChat()-> String{
        return url + "/api/chat/rooms"
    }
    public func addLessonCategories()-> String{
        return url + "/api/stages?includes=grades.sections,grades.subjects"
    }
    public func getNotifications()-> String{
        return url + "/api/notifications"
    }
    public func getLessons(date:String,subject:String,section:String,teacher:String)-> String{
        return url + "/api/lessons?created_at=\(date)&subject_id=\(subject)&section_id=\(section)&teacher_id=\(teacher)"
    }
    public func getLessons(id:String)-> String{
           return url + "/api/lessons/\(id)"
       }
    public func getFilteredData(stageId:String,grade_id:String,section_id:String)-> String{
           return url + "/api/chat/rooms?stage_id=\(stageId)&grade_id=\(grade_id)&section_id=\(section_id)"
       }
    public func getFilteredDataFromUserWithNoRoom(stageId:String,grade_id:String,section_id:String)-> String{
           return url + "/api/students?only_does_not_have_chat=1&stage_id=\(stageId)&grade_id=\(grade_id)&section_id=\(section_id)"
       }
    
    
}
