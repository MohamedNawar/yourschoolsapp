//
//  RemoteNotification.swift
//  YourSchools
//
//  Created by ElNoorOnline on 5/9/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation

struct RemoteNotification :Decodable {
    var  a : RemoteNotificationData?
    var unread_count:Int?
}

struct RemoteNotificationData :Decodable {
        var id      : Int?
        var name    : String?
        var view    : String?
    }
    
 
