//
//  LoginViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//
import Foundation
struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    var data : UserDetails?
    var token : String?
    var identifier : Int?
    var firstUse:String?
   
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "identifier")

        token = ""
    }
    
    mutating func removeIdentifierInside() {
        UserDefaults.standard.removeObject(forKey: "identifier")
    }
   
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "identifier") {
            self.identifier = Int(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "firstUse") {
            self.firstUse = String(data)
        }
    }
}
struct CurrentUser:Decodable {
    var data: UserDetails?
}
struct UserDetails : Decodable {
    var id:Int?
    var unseen_coversations_count:Int?
    var name : String?
    var email:String?
    var mobile:String?
    var type:String?
    var description:String?
    var school: School?
    var section:UserSection?
    var stage: String?
    var rate_student: Int?
    var preference_student: String?
    var about: String?
    var warnings: [Warning]?
    var avatar: String?
    var is_connected: Bool?
    var links:LinksData?
}
struct Warning:Decodable {
    var id: Int?
    var reason: String?
    var date: String?
}

struct UserSection:Decodable {
    var id: Int?
    var name: String?
    var grade: GradeData?
}
struct GradeData:Decodable {
    var id: Int?
    var name: String?
    var stage: StageData?
}
struct StageData:Decodable {
    var id: Int?
    var name: String?
}

struct School:Decodable {
    var id: Int?
    var name: String?
    var school_code: String?
    var logo: String?
}



