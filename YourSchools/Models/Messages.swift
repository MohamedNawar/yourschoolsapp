//
//  Messages.swift
//  YourSchools
//
//  Created by Mouhammed Ali on 4/5/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation




class Messages : Decodable {
    var data : [Message]?
    var links : Links?
    var meta : Meta?
    
}
class Messages2 : Decodable {
    var data : Message?
    var links : Links?
    var meta : Meta?
    
}

class Messages3 : Decodable {
    var message : Message?
    
}

class Message : Decodable {
    var id : Int?
    var socked_id : String?
    var body : String?
    var is_mine : Bool?
    var sender : UserDetails?
    var participant : UserDetails?
    var created_at : String?
    var seen_at : String?
    var is_first_in_its_day : Bool?
    
}

struct Links : Decodable {
    var first : String?
    var last : String?
    var prev : String?
    var next : String?
}

