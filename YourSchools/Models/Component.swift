//
//  LoginViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
import Foundation
struct Subjects: Decodable {
    var data: [SubjectData]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct SubjectData: Decodable {
    var id: Int?
    var name: String?
    var avatar: String?
    var newly_lessons_count: Int?
}
struct News: Decodable {
    var data:[NewsData]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct SchoolMedia: Decodable {
    var data:[SchoolMediaData]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct SchoolMediaData: Decodable {
    var id: Int?
    var name: String?
    var avatar: String?
    var ratio: Double?
    var album : [Image]?
}
struct Installments: Decodable {
    var paid: Paid?
    var not_paid: Paid?
    var partially_paid: Paid?
}
struct Paid: Decodable {
    var group: String?
    var status: String?
    var data: [PaidData]?
}
struct PaidData: Decodable {
    var date: String?
    var price: Int?
}

struct NewsData: Decodable {
    var id: Int?
    var title: String?
    var body: String?
    var is_urgent: Int?
    var views_count: Int?
    var ratio: Double?
    var avatar: String?
    var album: [albumData]?
    var created_at: String?
}
struct albumData: Decodable {
    var media_id: Int?
    var link: String?
}
struct Attendance: Decodable {
    var data:[AttendanceData]?
}
struct AttendanceData: Decodable {
    var day: String?
    var date: String?
    var status: String?
    var status_formated: String?
}
struct ExamsResults: Decodable {
    var data:[ExamsResultsData]?
}

struct ExamsResultsData: Decodable {
    var exam: String?
    var results: [Result]?
    var total: Int?
}
struct Result: Decodable {
    var subject: Subject?
    var mark: Int?
}
struct Subject: Decodable {
    var id: Int?
    var name: String?
    var avatar: String?
}
struct DirectorWord: Decodable {
    var data: DirectorWordData?
}
struct Flag: Decodable {
    var data: FlagData?
}
struct FlagData: Decodable {
    var lessons_count: Int?
    var news_count: Int?
    var unseen_rooms_count: Int?
    var unseen_adminstration_conversations_count: Int?
    var admin_word_count: Int?
    var attendance_count: Int?
    var installment_count: Int?
    var warning_count: Int?
    var school_media_count: Int?
    var schedules_count: Int?
    var result_count: Int?
    var exams_count: Int?
}
struct DirectorWordData: Decodable {
    var adminName: String?
    var adminWord: String?
    var avatar: Image?
    enum CodingKeys:String, CodingKey {
        case adminName = "admin-name"
        case adminWord = "admin-word"
        case avatar = "image"
    }
}
struct AdminstrationRoom: Decodable {
    var data: [AdminstrationRoomData]?
    var links: ServicesLinks?
    var meta: Meta?
    var room: Room?
}
struct Rooms: Decodable {
    var data: [Room]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct Lessons: Decodable {
    var data: [LessonData]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct LessonData: Decodable {
    var lessons: [LessonContentData]?
    var date: String?
}
struct LessonContentData: Decodable {
    var id: Int?
    var name: String?
    var is_new: Bool?
    var description: String?
    var youtube_id: String?
    var created_at: String?
    var images: [Image]?
    var attachments: [Image]?
    var voices: Image?
    var video: Image?
    var audio: Image?
    var teacher: UserDetails?
    var section: Section?
    var subject: SubjectData?
}

struct Exams: Decodable {
    var data: [ExamData]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct Exam: Decodable {
    var data: ExamData?
}
struct ExamData: Decodable {
    var id: Int?
    var name: String?
    var subjects: [SubjectExamDetailsData]?
}
struct Image: Decodable {
    var url: String?
    var type: String?
    var delete_url: String?
    var mime_type:String?
}
struct SubjectExamDetailsData: Decodable {
    var subject: SubjectData?
    var day: String?
    var date: String?
    var start_at: String?
    var end_at: String?
    var time_formated: String?
}
struct Notifications: Decodable {
    var count: Int?
    var link: String?
    var data: [NotificationData]?
}
struct NotificationData: Decodable {
    var id: String?
    var avatar: String?
    var title: String?
    var body: String?
    var dashboard_url: String?
    var url: String?
    var date: DateData?
    var formated_date: String?
    var data_view: String?
    var data_id: Int?
}
struct DateData: Decodable {
    var date: String?
    var timezone_type: Int?
    var timezone: String?
    
}
struct Room: Decodable {
    var room_id: Int?
    var subscribers: [Subscriber]?
    var name: String?
    var avatar: String?
    var type: String?
    var lastConversation: AdminstrationRoomData?
    var can_join: Bool?
    var count_unseen: Int?
    var links: LinksData?
    var id:Int?

    
}
struct LinksData: Decodable {
    var send_message: DeleteMessage?
    var conversation: DeleteMessage?
}
struct Subscriber: Decodable {
    var id: Int?
    var name: String?
    var avatar: String?
    var last_seen: LastSeen?
    var is_connected : Bool?
}
struct LastSeen: Decodable {
    var conversation_id: Int?
    var seen_at: String?
    var seen_at_formated: String?
}
struct AdminstrationRoomData: Decodable {
    var id: Int?
    var sender: Sender?
    var message: String?
    var date: String?
    var created_at: String?
    var created_at_formated: String?
    var is_seen: Bool?
    var links: DeleteMessage?
}
struct DeleteMessage: Decodable {
    var url: String?
    var type: String?
}
struct Sender: Decodable {
    var id: Int?
    var name: String?
    var type: String?
    var avatar: String?
}
struct Schedule: Decodable {
    var data: [ScheduleData]?
}
struct AddLessonCategories: Decodable {
    var data: AddLessonData?
}
struct AddLessonData: Decodable {
    var id: Int?
    var name: String?
    var email: String?
    var options: [Stage]?
    var label:String?
}
struct Stage: Decodable {
    var id: Int?
    var name: String?
    var grades: Grade1?
}
struct Grade1: Decodable {
    var options: [Grade]?
}
struct Section1: Decodable {
    var options: [Section]?
}
struct SubjectData1: Decodable {
    var options: [SubjectData]?
}
struct Grade: Decodable {
    var id: Int?
    var name: String?
    var sections:Section1?
    var subjects: SubjectData1?
    var stage: Stage?
}
struct Section: Decodable {
    var id: Int?
    var name: String?
    var grade:Grade?
}
struct ScheduleData: Decodable {
    var day: String?
    var start_at: String?
    var localed_sort: String?
    var end_at: String?
    var time_formated: String?
    var teacher: UserDetails?
    var section: Section?
    var subject: SubjectData?
    var links: DeleteMessage?
}
struct AllTeachers: Decodable {
    var data: [UserDetails]?
    var links: ServicesLinks?
    var meta: Meta?
}
struct CurrentUserData: Decodable {
    var data: UserDetails?
    var token: String?
}
class ServicesLinks: Decodable {
    var first: String?
    var last: String?
    var prev: String?
    var next: String?
}
class Meta: Decodable {
    var current_page: Int?
    var from: Int?
    var last_page: Int?
    var path: String?
    var per_page: Int?
    var to: Int?
    var total: Int?
    
}

struct AllRooms: Decodable {
    var data: [Room]?
    var links: ServicesLinks?
    var meta: Meta?
}


