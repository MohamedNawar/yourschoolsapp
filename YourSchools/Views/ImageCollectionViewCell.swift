//
//  ImageCollectionViewCell.swift
//  YourSchools
//
//  Created by iMac on 11/7/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    var DeleteCallBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func removeFromCollection(_ sender: Any) {
        DeleteCallBack!()
    }
}
