//
//  fileCollectionViewCell.swift
//  YourSchools
//
//  Created by Mohamed Hassan Nawar on 12/29/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class fileCollectionViewCell: UICollectionViewCell {
    var DeleteCallBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func removeFromCollection(_ sender: Any) {
        DeleteCallBack!()
    }
    
}

