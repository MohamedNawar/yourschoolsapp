//
//  NewsCell.swift
//  YourSchools
//
//  Created by iMac on 12/11/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
   var shareCallBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    @IBAction func share(_ sender: Any) {
        shareCallBack?()
    }
}
