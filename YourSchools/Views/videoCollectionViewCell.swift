//
//  videoCollectionViewCell.swift
//  YourSchools
//
//  Created by MACBOOK on 11/12/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class videoCollectionViewCell: UICollectionViewCell {
    var DeleteCallBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func removeFromCollection(_ sender: Any) {
        DeleteCallBack!()
    }

}
