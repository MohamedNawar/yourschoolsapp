//
//  AppDelegate.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FCAlertView
import LanguageManager_iOS
import OneSignal
import UserNotifications
import Alamofire
//let NormalFont = UIFont(name: "neosans_regular", size: 17)
//let BoldFont = UIFont(name: "neosans_regular", size: 17)
let mainColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , OSSubscriptionObserver {
    var remoteNotification = RemoteNotification()
    /* one signal */
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        //print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "onesignalid")
         //   print("ooo \(UserDefaults.standard.string(forKey: "onesignalid"))")
            print("Current playerId \(playerId)")
            
            
        }
    }
    
    func notification(){
        let content = UNMutableNotificationContent()
        content.badge = 1
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        let request = UNNotificationRequest(identifier: "", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let application = UIApplication.shared
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge , .alert]) { (success, error) in
            if error == nil {
                print("Notification Allow...")
            }
      
        }
               // for badge
               resetNotificationBadge()

       print("************** \(userInfo)")
//
        let state : UIApplication.State = application.applicationState
        if (state == .inactive || state == .background) {
            // go to screen relevant to Notification content

            do{

                if  let data = userInfo["custom"] {

                    let json = try JSONSerialization.data(withJSONObject: data, options: [])
                    let result_  = try JSONDecoder().decode(RemoteNotification.self, from: json)


                    notification_view = result_.a?.view ?? ""
                    print(result_.a?.view ?? "")
                    print("/**//**/*/*/*//**/*//**/*/*/*/*/")
                                            NotificationCenter.default.post(name: Notification.Name("NotificationComming"), object: nil)
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainSectionsViewController") as! MainSectionsViewController
                    initialViewController.notification_view = notification_view
                    initialViewController.remote_notification_data = result_.a
                    let vcc : UINavigationController = UINavigationController(rootViewController: initialViewController)
                    self.window?.rootViewController = vcc
                    self.window?.makeKeyAndVisible()
 
                }
            }catch{
                print(error)
            }

        } else {

            print("app is active")
        }
    }
    func setupBadgeNumberPermissions() {
        let types: UIUserNotificationType = UIUserNotificationType.badge
        let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(types: types, categories: nil)
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
    }
    
    var notification_view = ""
    var window: UIWindow?
    private var reachability : Reachability!
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var navigationController = UINavigationController()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UserDefaults.standard.set(0, forKey: "badgeCount")
        UNUserNotificationCenter.current().delegate = self
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.applicationIconBadgeNumber = 0;
        setupBadgeNumberPermissions()
        let badgeCount: Int = 10
        let application = UIApplication.shared
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = badgeCount
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge , .alert]) { (success, error) in
            if error == nil {
                print("Notification Allow...")
                self.notification()
            }
        }
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
//        UserDefaults.standard.removeObject(forKey: "firstUse")
         //LanguageManager.shared.setLanguage(language: Languages(rawValue: Locale.current.languageCode!)!)
        LanguageManager.shared.setLanguage(language: Languages(rawValue: Locale.current.languageCode!)!)
        userData.Instance.fetchUser()
        if userData.Instance.firstUse == "" || userData.Instance.firstUse == nil {
            UserDefaults.standard.set("false", forKey: "firstUse")
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()

        }else{
        if userData.Instance.token == "" || userData.Instance.token == nil {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerNV")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }else{
            

            
            if let type = userData.Instance.data?.type {
                if type == "teacher" {
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "InstructorPageViewControllerNV")
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()
                }
//                else{
//                    self.window = UIWindow(frame: UIScreen.main.bounds)
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainSectionsViewController") as! MainSectionsViewController
//                    initialViewController.notification_view = "tttttttteteteetetetetetetteteetet"
//                     let vcc : UINavigationController = UINavigationController(rootViewController: initialViewController)
//                    self.window?.rootViewController = vcc
//                    self.window?.makeKeyAndVisible()
//                }
            }
        }
        }
        // Override point for customization after application launch.
//        L102Localizer.DoTheMagic()
        UIApplication.shared.statusBarStyle = .lightContent
        let ToolBarAppearace = UIToolbar.appearance()
        let BarAppearace = UINavigationBar.appearance()
        //        BarAppearace.barTintColor = .clear
        BarAppearace.setBackgroundImage(UIImage(), for: .default)
        BarAppearace.shadowImage = UIImage()
        BarAppearace.isTranslucent = true
        
//        BarAppearace.titleTextAttributes = [
//            NSAttributedString.Key.font: NormalFont ?? UIFont(name: "neosans_regular", size: 17) ,
//            NSAttributedString.Key.foregroundColor: UIColor.white
//        ]
        ToolBarAppearace.backgroundColor = mainColor
        ToolBarAppearace.tintColor = UIColor.white
        let ButtonBarAppearace = UIBarButtonItem.appearance()
//        ButtonBarAppearace.setTitleTextAttributes([
//            NSAttributedString.Key.font: NormalFont as Any
//            ], for: UIControl.State.normal)
        ButtonBarAppearace.tintColor = UIColor.white
        
        //Keybored Setup
        IQKeyboardManager.shared.enable = true
        
        //For checking the Internet
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        self.reachability = Reachability.init()
        do {
            try self.reachability.startNotifier()
        } catch {
            
        }

        /* one signal notification */
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "dc50399b-7c5e-4934-8777-8e4b9b07ff55",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        OneSignal.add(self as OSSubscriptionObserver)
        
        return true
    }

        func applicationWillResignActive(_ application: UIApplication) {
               // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
               // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
           }

           func applicationDidEnterBackground(_ application: UIApplication) {
               // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
               // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
           }

           func applicationWillEnterForeground(_ application: UIApplication) {
               // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    //        UserDefaults.standard.set(0, forKey: "badgeCount")
           }

           func applicationDidBecomeActive(_ application: UIApplication) {
            resetNotificationBadge()
               // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

           }

           func applicationWillTerminate(_ application: UIApplication) {
               // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //        UserDefaults.standard.set(0, forKey: "badgeCount")
    //        application.applicationIconBadgeNumber = 0


           }
    //Get called when the wifi statue changed
    @objc func reachabilityChanged(notification:Notification) {
        let reachability = notification.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            makeDoneAlert(title: "من فضلك قم باعادة الاتصال بالانترنت مرة اخري", SubTitle: "", Image: #imageLiteral(resourceName: "Untitled-11"), color: UIColor.red)
            print("Network not reachable")
        }
    }
    
    // the alert to be Poped Up
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // Print full message.
        print(userInfo)
    }
    
    func resetNotificationBadge(){
        userData.Instance.fetchUser()
        let header = APIs.Instance.getHeader()
        print(header)
        print(APIs.Instance.resetNotificationBadge())
        Alamofire.request(APIs.Instance.resetNotificationBadge() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.remoteNotification = try JSONDecoder().decode(RemoteNotification.self, from: response.data!)
                        print(self.remoteNotification.unread_count ?? 0)
                        let countOfUnreadNotification = UserDefaults.standard.integer(forKey: "unReadNotify")
                        UserDefaults.standard.set(countOfUnreadNotification + (self.remoteNotification.unread_count ?? 0), forKey: "unReadNotify")
                        NotificationCenter.default.post(name: Notification.Name("NotificationBadge"), object: nil)

                    }catch{
                        print(error)
                    }
                }
            case .failure(_):
                break
            }
        }
    }
}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("uyu")
    }
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        //
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        //completionHandler([userInfo])
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
