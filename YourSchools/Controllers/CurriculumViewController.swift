//
//  CurriculumViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import LanguageManager_iOS

class CurriculumViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var tableView: UITableView!
    var day = Int(){
        didSet{
            loadCurriculumCurrentDay(day: day)
        }
    }
    
    var schedule = Schedule(){
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedule.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurriculumCell", for: indexPath)
         let lbl1 = cell.viewWithTag(1) as! UILabel
         let lbl2 = cell.viewWithTag(2) as! UILabel
         let lbl3 = cell.viewWithTag(3) as! UILabel
         let lbl4 = cell.viewWithTag(4) as! UILabel
         let lbl5 = cell.viewWithTag(770) as! UILabel
         let lbl6 = cell.viewWithTag(760) as! UILabel
         let view = cell.viewWithTag(5) as! UIView
         let teacherView = cell.viewWithTag(440) as! UIStackView
         let sectionView = cell.viewWithTag(441) as! UIStackView
         let educatinalLevelView = cell.viewWithTag(442) as! UIStackView

        if let curriculum = schedule.data?[indexPath.row] {
            lbl1.text = curriculum.day ?? ""
            lbl2.text = curriculum.subject?.name ?? ""
            lbl3.text = curriculum.teacher?.name ?? ""
            lbl4.text = curriculum.localed_sort ?? ""
            lbl5.text = curriculum.section?.grade?.name ?? ""
            lbl6.text = curriculum.section?.name ?? ""
        }
        if LanguageManager.shared.currentLanguage == .ar {
           lbl1.textAlignment = .right
           lbl2.textAlignment = .right
           lbl3.textAlignment = .right
           lbl4.textAlignment = .right
           lbl5.textAlignment = .right
           lbl6.textAlignment = .right

        }
         if let dayValue = schedule.data?[indexPath.row].day {
            switch dayValue {
            case "السبت":
                view.backgroundColor = #colorLiteral(red: 0, green: 0.3285208941, blue: 0.5748849511, alpha: 1)
            case "الأحد":
                view.backgroundColor = #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)
            case "الأثنين":
                view.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            case "الثلاثاء":
                view.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
            case "الأربعاء":
                view.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            case "الخميس":
                view.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
            case "الجمعة":
                view.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
                break
            default: ""
            }
        }
        if userData.Instance.data?.type ?? "" == "teacher" {
                  teacherView.isHidden = true
        }else{
            sectionView.isHidden = true
            educatinalLevelView.isHidden = true
        }
        return cell
    }
   private func loadCurriculumCurrentDay(day:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.scheduleForDay(day: day) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.schedule = try JSONDecoder().decode(Schedule.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
}
