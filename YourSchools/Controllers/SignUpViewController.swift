//
//  SignUpViewController.swift
//  YourSchools
//
//  Created by iMac on 11/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var schoolCode: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var password1lTextField: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
              self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.clear]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        schoolCode.delegate = self
        passwordTextField.delegate = self
        emailTextField.delegate = self
        phoneTextField.delegate = self
        userName.delegate = self
        password1lTextField.delegate = self
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        schoolCode.addPadding(UITextField.PaddingSide.left(20))
        passwordTextField.addPadding(UITextField.PaddingSide.left(20))
        userName.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
        password1lTextField.addPadding(UITextField.PaddingSide.left(20))
        
    }
    override func viewDidLayoutSubviews() {
        self.loopThroughSubViewAndAlignTextfieldText(subviews: self.view.subviews)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0);
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 0.6687714041)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    func PostProfileInfo(){
        if self.schoolCode.text == "" || self.emailTextField.text == "" || self.phoneTextField.text == "" || self.userName.text == "" ||
            self.passwordTextField.text == "" || self.password1lTextField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
                if self.passwordTextField.text != self.password1lTextField.text {
                    HUD.flash(.label("Your Password Not Identical"), delay: 1.0)
                    return
                }
        let header = APIs.Instance.getHeader()
        let par = ["name": userName.text!, "email": emailTextField.text! , "mobile": phoneTextField.text!, "password": passwordTextField.text!, "code": schoolCode.text!] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("successsss")
                        UserDefaults.standard.set(0, forKey: "unReadNotify")
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func SignUpBtn(_ sender: Any) {
        
    }
    @IBAction func SignIn(_ sender: Any) {
    }
    
    
}



