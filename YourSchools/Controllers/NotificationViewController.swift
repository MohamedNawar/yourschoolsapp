//
//  NotificationViewController.swift
//  YourSchools
//
//  Created by iMac on 12/30/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SDWebImage
import LanguageManager_iOS

class NotificationViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var tableView: UITableView!
    var tag = Int()
    var notifications = Notifications(){
        didSet{
//            let count = self.notifications.count ?? 0
//            if count > 0 {
//                for i in 0..<count{
//                    seeNotification(url: notifications.data?[i].id ?? "")
//                }
//            }
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UserDefaults.standard.set(0, forKey: "unReadNotify")
        userData.Instance.fetchUser(); self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        getNotifications()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
        let lbl1 = cell.viewWithTag(1) as! UILabel
        let lbl2 = cell.viewWithTag(2) as! UILabel
        let lbl3 = cell.viewWithTag(11) as! UILabel
        let lbl4 = cell.viewWithTag(111) as! UILabel
        let imageView = cell.viewWithTag(4) as! UIImageView
        let view1 = cell.viewWithTag(121) as! UIView
        if let notificationsData = notifications.data {
            if LanguageManager.shared.currentLanguage.rawValue == "ar" {
                lbl1.textAlignment = .right
                lbl2.textAlignment = .right

            }else{
                lbl1.textAlignment = .left
                lbl2.textAlignment = .left

            }
            //                if let count = teachersData[indexPath.row].unseen_coversations_count {
            //                    if count > 0 {
            //                        view1.isHidden = false
            //                    }
            //                }
            lbl1.text = notificationsData[indexPath.row].title ?? ""
            lbl2.text = notificationsData[indexPath.row].body ?? ""
            let isoDate = notificationsData[indexPath.row].date?.date ?? ""
            lbl3.text = "\(isoDate.toDate(format: "yyyy-MM-dd HH:mm:ss.SSSSSS")?.asString() ?? "")"
            lbl4.text = notificationsData[indexPath.row].formated_date ?? ""
//            if let  imageIndex = notificationsData[indexPath.row].avatar{
//                imageView.sd_setImage(with: URL(string: imageIndex), completed: nil)
//            }
            imageView.sd_setImage(with: URL(string: userData.Instance.data?.school?.logo ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))
        }
        return cell
    }
    private func getNotifications(){
        let header = APIs.Instance.getHeader()
        print(header)
        print(APIs.Instance.getNotifications())
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.getNotifications(), method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.notifications = try JSONDecoder().decode(Notifications.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    private func seeNotification(url:String){
        var url1 = "http://service.madariskm.com/api/notifications?notification_id=\(url)"
        let header = APIs.Instance.getHeader()
        print(url1)
        Alamofire.request(url1, method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                   
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.notifications = try JSONDecoder().decode(Notifications.self, from: response.data!)
                    }catch{
                        print(error)
                    }
                }
            case .failure(_):
           
                break
            }
        }
    }
    func remote_notification_action(index:IndexPath , notification_view:String){
                switch notification_view {
                    case "lesson":
                   let navVC = storyboard?.instantiateViewController(withIdentifier: "DialyDutiesViewController") as! DialyDutiesViewController
                    self.navigationController?.pushViewController(navVC, animated: true)
                    
                    
    //               if let currntLessonData = lessons.data?[indexPath.section].lessons?[indexPath.row-1] {
    //                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowLessonViewController") as! ShowLessonViewController
    //                navVC.lessonContent = currntLessonData
    //                self.navigationController?.pushViewController(navVC, animated: true)
    //                }
                    
                    
                case "attendance":
                        let navVC = storyboard?.instantiateViewController(withIdentifier: "AttendanceAndAbsenceViewController") as! AttendanceAndAbsenceViewController
                        navVC.title = NSLocalizedString( "Attendance", comment: "")
                    self.navigationController?.pushViewController(navVC, animated: true)

                case "schedule":
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
                    navVC.title = NSLocalizedString("School Schedule", comment: "")
                    navVC.tag = 0
                    self.navigationController?.pushViewController(navVC, animated: true)
                    
                case "exams":
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
                    navVC.title = NSLocalizedString("Exams Table", comment: "")
                    navVC.tag = 1
                    self.navigationController?.pushViewController(navVC, animated: true)
                    
                case "result":
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "QuizResultViewController") as! QuizResultViewController
                    navVC.title = NSLocalizedString("Exams Results", comment: "")
                    self.navigationController?.pushViewController(navVC, animated: true)
                    

     
                case "adminstration":
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactManagementViewController") as! ContactManagementViewController
                    navVC.title =  NSLocalizedString("Contact Management", comment: "")
                    self.navigationController?.pushViewController(navVC, animated: true)

//                case "conversation":
//    //
//
//                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactSelectedTeacherViewController") as! ContactSelectedTeacherViewController
//                    navVC.title =  NSLocalizedString("Teacher Conversation", comment: "")
//                    navVC.teacherFlag = "teacher"
//                    navVC.selectedUserName = remote_notification_data?.name ?? ""
//                    navVC.id = remote_notification_data?.id ?? 0
//                    self.navigationController?.pushViewController(navVC, animated: true)
                    
      
                case "news":
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolNewsViewController") as! SchoolNewsViewController
                    navVC.title = NSLocalizedString("School News", comment: "")
                    self.navigationController?.pushViewController(navVC, animated: true)

                case "school_media" :
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as! ImageCollectionViewController
                    navVC.title = NSLocalizedString("Photo Gallery", comment: "")
                    self.navigationController?.pushViewController(navVC, animated: true)
                    case "" :
                    print("test")
                    
                    default:
                    break
                }
            }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        remote_notification_action(index: indexPath, notification_view: notifications.data?[indexPath.row].data_view ?? "")
//        seeNotification(url: notifications.data?[indexPath.row].id ?? "")
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
