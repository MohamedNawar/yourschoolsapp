//
//  ConfirmationDoneViewController.swift
//  YourSchools
//
//  Created by Mouhammed Ali on 4/7/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class ConfirmationDoneViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
//        mytapGestureRecognizer.numberOfTapsRequired = 1
//        mytapGestureRecognizer.cancelsTouchesInView = true
//        self.view.addGestureRecognizer(mytapGestureRecognizer)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss(_ sender: Any) {
       
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController =   storyboard.instantiateViewController(withIdentifier: "InstructorPageViewControllerNV")
        self.navigationController?.navigationBar.barStyle = .blackTranslucent

        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
        
    }
    
//    @objc func myTapAction(recognizer: UITapGestureRecognizer) {
//        self.dismiss(animated: true, completion: nil)
//    }
    
}

