//
//  TutorialStepViewController.swift
//  YourSchools
//
//  Created by iMac on 11/8/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class TutorialStepViewController: UIViewController {
    
   // @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var textLabel: UILabel!
    
    var backgroundImage: UIImage?
    var iconImage: UIImage?
    var text: String?
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        backgroundImageView.image = backgroundImage
        iconImageView.image = iconImage
        textLabel.text = text
      
    }
//    @objc func methodOfReceivedNotification(notification: Notification) {
//        skipBtn.isHidden = false
//        skipBtn.isEnabled = true
//    }
////    @IBAction func skipeSteps(_ sender: Any) {
////        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
////        self.navigationController?.pushViewController(navVC, animated: true)
////
////    }
}

