//
//  DailyAssignmentViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView

class DailyAssignmentViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var tableView: UITableView!
    var section = Int()
    var currentSubjectId = Int(){
        didSet{
            print(section)
            print(currentSubjectId)
        }
    }
    var selectedIndex = -1
    var lessons = Lessons(){
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         tableView.reloadData()
        getLessons(id: section)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) ;
    
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = lessons.data?[section].lessons?.count ?? 0
        return count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DailyAssignmentHeader", for: indexPath)
            let lbl = cell.viewWithTag(7) as! UILabel
            lbl.text = lessons.data?[indexPath.section].date ?? ""
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyAssignmentCell", for: indexPath)
        let lbl1 = cell.viewWithTag(1) as! UILabel
//        let lbl2 = cell.viewWithTag(2) as! UILabel
        let image = cell.viewWithTag(3) as! UIImageView
        let view = cell.viewWithTag(10) as! UIView
        if let currntLessonData = lessons.data?[indexPath.section].lessons?[indexPath.row-1] {
            if currntLessonData.is_new == true {
                view.isHidden = false
            }else{
                view.isHidden = true

            }
            if selectedIndex != -1 {
                if selectedIndex == indexPath.row-1 {
                    view.isHidden = true
                }
            }
            lbl1.text = "\(currntLessonData.name ?? "")\n\(currntLessonData.description ?? "")\n\n\(currntLessonData.teacher?.name ?? "")"
//            lbl2.text = currntLessonData.teacher?.name ?? ""
            image.sd_setImage(with: URL(string: currntLessonData.images?.first?.url ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))
            return cell

        }

        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return lessons.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if let currntLessonData = lessons.data?[indexPath.section].lessons?[indexPath.row-1] {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowLessonViewController") as! ShowLessonViewController
            navVC.title = NSLocalizedString("Lesson", comment: "")
            navVC.lessonContent = currntLessonData
            self.selectedIndex = indexPath.row-1
            self.lessons.data?[indexPath.section].lessons?[indexPath.row-1].is_new = false
        self.navigationController?.pushViewController(navVC, animated: true)
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    private func getLessons(id: Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.getSubjects())
        print (APIs.Instance.getLessons(id: id, subjectId: currentSubjectId))
        
        Alamofire.request(APIs.Instance.getLessons(id: id, subjectId: currentSubjectId) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.lessons = try JSONDecoder().decode(Lessons.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
