//
//  MangerWordViewController.swift
//  YourSchools
//
//  Created by iMac on 11/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//
import UIKit
import Alamofire
import PKHUD
import FCAlertView
class MangerWordViewController: CustomBaseVC {
    var adminWord = DirectorWord(){
        didSet{
            directorName.text = adminWord.data?.adminName
            directorWord.text = adminWord.data?.adminWord
        
        }
    }
    @IBOutlet weak var imageDirector: UIImageView!
    @IBOutlet weak var directorName: UILabel!
    @IBOutlet weak var directorWord: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        AdminWord()
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    private func AdminWord(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.directorWord(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                         self.adminWord = try JSONDecoder().decode(DirectorWord.self, from: response.data!)
                        print(self.adminWord.data?.avatar?.url ?? "")
                        self.imageDirector.sd_setImage(with: URL(string: self.adminWord.data?.avatar?.url ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))
                    }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }

}
