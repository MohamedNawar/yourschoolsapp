//
//  ContactManagementViewController.swift
//  YourSchools
//
//  Created by iMac on 11/10/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import JSQMessagesViewController
import RSSelectionMenu
import PusherSwift
import LanguageManager_iOS
 
class ContactManagementViewController: JSQMessagesViewController, PusherDelegate {
    let pusher = Pusher(
        key: "c3d7286b7ecbdc07be63",
        options: PusherClientOptions(
            authMethod: .inline(secret: "449f204f6c63c6ebdafc") ,
            host: .cluster("eu")

        )
    )

    
   
    var user1 = userData.Instance.data
    var currentUserChat = AdminstrationRoom(){
        didSet{
           
            self.messages = getMessages()
            
        }
    }
    var currentUser: UserDetails? {
        return user1
    }
    
    // all messages of users1, users2
    var messages = [JSQMessage](){
        didSet{
            self.collectionView.reloadData()
        }
    }
}

extension ContactManagementViewController {
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        sendMessage(id: "\(currentUserChat.room?.room_id ?? 0)" , message: text)
        
        finishSendingMessage()
    }
    private func sendMessage(id:String , message:String){
        let header = APIs.Instance.getHeader()
        let par = ["message": message] as [String : String]
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.sendMessage(id: id), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print("successsss")
                     }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        
        return NSAttributedString(string: messageUsername!)
    }
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
    //        messages.remove(at: indexPath.row)
    //    }
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
    //            let simpleDataArray = ["Delete"]
    //            let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
    //                if indexPath.row == 0{
    //                    cell.textLabel?.text = object
    //                }
    //            }
    //
    //
    //            selectionMenu.onDismiss = {(text) in
    //                    print("onDismiss")
    //            }
    //        selectionMenu.show(style: .Popover(sourceView: self.collectionView.cellForItem(at: indexPath) ?? UICollectionViewCell(), size: CGSize(width: 80, height: 2)), from: self)
    //    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let imageView = UIImageView()
        if let  img = currentUserChat.data?[indexPath.row].sender?.avatar {
            print(img)
            imageView.sd_setImage(with: URL(string: img ), placeholderImage: #imageLiteral(resourceName: "img34"))
            if (imageView.image != nil) {
                let avatar = JSQMessagesAvatarImageFactory.avatarImage(with: imageView.image, diameter: UInt(40));
                return avatar
                
            }else{
                let avatar = JSQMessagesAvatarImageFactory.avatarImage(with: #imageLiteral(resourceName: "img20"), diameter: UInt(40));
                return avatar
            }
        }else{
            return nil
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row]
        
        if currentUser?.id == Int(message.senderId) {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
}

extension ContactManagementViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showChat()

        // tell JSQMessagesViewController
        // who is the current user
        self.senderId = String(currentUser?.id ?? 0)
        self.senderDisplayName = currentUser?.name
        
        
        self.messages = getMessages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        self.inputToolbar.contentView.leftBarButtonItem = nil;
        self.title = NSLocalizedString("Contact Management", comment: "")
            if LanguageManager.shared.currentLanguage == .ar {
                self.inputToolbar.contentView.textView.placeHolder = "رسالة جديدة"
                self.inputToolbar.contentView.textView.textAlignment = .right; self.inputToolbar.contentView.rightBarButtonItem.setTitle("إرسال", for: .normal)
            }else if LanguageManager.shared.currentLanguage == .ku {
                self.inputToolbar.contentView.textView.placeHolder = "Peyamek nû"
                self.inputToolbar.contentView.textView.textAlignment = .left
                self.inputToolbar.contentView.rightBarButtonItem.setTitle("şand", for: .normal)
            }else{
                self.inputToolbar.contentView.textView.placeHolder = "New Message"
                self.inputToolbar.contentView.textView.textAlignment = .left
                self.inputToolbar.contentView.rightBarButtonItem.setTitle("Send", for: .normal)
            }
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    override func viewWillDisappear(_ animated: Bool) {
        if LanguageManager.shared.currentLanguage.rawValue == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft

        } else if LanguageManager.shared.currentLanguage.rawValue == "en"{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
    }
    private func showChat(){
        let header = APIs.Instance.getHeader()
        print(APIs.Instance.showChatWithAdmin())
        print(APIs.Instance.getHeader())
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showChatWithAdmin(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image:#imageLiteral(resourceName: "Untitled-11") )
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        self.currentUserChat = try JSONDecoder().decode(AdminstrationRoom.self, from: response.data!)
                        self.setupPusher()
                        //                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                        //                        self.dismiss(animated: true, completion: nil)
                    }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}

extension ContactManagementViewController {
    func getMessages() -> [JSQMessage] {
        var messages = [JSQMessage]()
        if let chatArray =  currentUserChat.data {
            for chat in chatArray {
                var message = JSQMessage(senderId: "\(chat.sender?.id ?? 0)", displayName: "\(chat.sender?.name ?? "")", text: "\(chat.message ?? "")")
                print(message)
                //                messages.append(message!)
                messages.insert(message!, at: 0)
            }
        }
        //        let message1 = JSQMessage(senderId: "1", displayName: "Steve", text: "Hey Tim how are you?")
        //        let message2 = JSQMessage(senderId: "2", displayName: "Tim", text: "Fine thanks, and you?")
        //
        //        messages.append(message1!)
        //        messages.append(message2!)
        
        return messages
    }
    func setupPusher() {
        let onMemberAdded = { (member: PusherPresenceChannelMember) in
            if member.userInfo == nil{
                return
            }
            //  HUD.flash(.label("المستخدم نشط حالياً"), delay: 1.0)
        }
        let onMemberRemoved = { (member: PusherPresenceChannelMember) in
            if member.userInfo == nil{
                return
            }
            // HUD.flash(.label("المستخدم ليس نشط حاليا"), delay: 1.0)
            
        }
        
        let channel = pusher.subscribe("presence-chat.\(self.currentUserChat.room?.room_id ?? 0)", onMemberAdded: onMemberAdded, onMemberRemoved: onMemberRemoved)
        
        channel.bind(eventName: "pusher:subscription_succeeded", callback: { data in
            print("Subscribed!")
            
        })
        
        
        channel.bind(eventName: "App\\Events\\ConversationSent", callback: { (data: Any?) -> Void in
            
            print(data)
            do{
                
                if data != nil {
                    let json = try JSONSerialization.data(withJSONObject: data!, options: [])
                    let chat = try JSONDecoder().decode(AdminstrationRoomData.self, from: json)
                    
                    self.currentUserChat.data?.insert(chat, at: 0)

                    self.scroll(to: IndexPath(row: self.currentUserChat.data?.count ?? 0, section: 0), animated:   false)
                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                    
                }
            }
            catch{
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
            }
        })
        
        pusher.connection.delegate = self
        pusher.connect()
        
    }
    func jsonToData(json: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }

}

