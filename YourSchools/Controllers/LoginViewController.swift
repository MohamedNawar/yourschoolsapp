//
//  LoginViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import RSSelectionMenu
import LanguageManager_iOS
class LoginViewController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var langBtn: UIButton!
    @IBOutlet weak var schoolCode: UITextField!
    @IBOutlet weak var passwordlTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var currentUser = CurrentUserData()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.clear]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        schoolCode.delegate = self
        passwordlTextField.delegate = self
        emailTextField.delegate = self
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        schoolCode.addPadding(UITextField.PaddingSide.left(20))
        passwordlTextField.addPadding(UITextField.PaddingSide.left(20))
        
        
    }
    ////login
    override func viewDidLayoutSubviews() {
        self.loopThroughSubViewAndAlignTextfieldText(subviews: self.view.subviews)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if LanguageManager.shared.currentLanguage.rawValue == "ar" {
            langBtn.setTitle("اللغة", for: .normal)
        } else if LanguageManager.shared.currentLanguage.rawValue == "en"{
            langBtn.setTitle("lan", for: .normal)
        }else{
            langBtn.setTitle("ku", for: .normal)
        }
        userData.Instance.fetchUser()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0);
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 0.6687714041)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    private func userLogin(){
        if self.schoolCode.text == "" || self.emailTextField.text == "" || self.passwordlTextField.text == "" {
            HUD.flash(.label("Enter your Data".localized()), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        
        let par = ["user_code": emailTextField.text!,
                   "password": passwordlTextField.text! ,
                   "school_code": schoolCode.text!,
                   "onesignal-player-id" : UserDefaults.standard.string(forKey: "onesignalid") ?? ""
            ] as [String : String]
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.login(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        self.currentUser = try JSONDecoder().decode(CurrentUserData.self, from: response.data!)
                        UserDefaults.standard.set(0, forKey: "unReadNotify")
                        print("successsss")
                        if let type = self.currentUser.data?.type {
                            print(type)
                            if type == "teacher" {
                                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "InstructorPageViewController") as! InstructorPageViewController
                                self.navigationController?.pushViewController(navVC, animated: true)
                                
                            }else{
                                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MainSectionsViewController") as! MainSectionsViewController
                                self.navigationController?.pushViewController(navVC, animated: true)
                            }
                        }else{
                            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MainSectionsViewController") as! MainSectionsViewController
                            self.navigationController?.pushViewController(navVC, animated: true)
                            
                        }
                        //                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                        //                        self.dismiss(animated: true, completion: nil)
                    }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func LoginBtn(_ sender: Any) {
        userLogin()
    }
    @IBAction func forgotPassword(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    @IBAction func langButtSlected(_ sender: UIButton) {
        let simpleDataArray = ["عربي","English","Kurds"]
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            if indexPath.row == 0{
                cell.textLabel?.text = object
            }else{
                cell.textLabel?.text = object
            }
        }
        
        
        selectionMenu.onDismiss = {(text) in
            if text.first == "عربي"{
                self.changeLang(lang: "ar")
            }else if text.first == "English" {
                self.changeLang(lang: "en")
            }else{
                self.changeLang(lang: "ku")
            }
            
        }
        
        // show as PresentationStyle = Push
        //selectionMenu.show(style: .Alert(title: "", action: "Done", height: 300), from: self)
        selectionMenu.show(style: .Popover(sourceView: sender, size: CGSize(width: 250, height: 100)), from: self)
    }
    
    func changeLang(lang:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
        if userData.Instance.firstUse == "" || userData.Instance.firstUse == nil {
            UserDefaults.standard.set("false", forKey: "firstUse")
            initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
            
        }else{
            if userData.Instance.token == "" || userData.Instance.token == nil {
                initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerNV")
            }else{
                if let type = userData.Instance.data?.type {
                    if type == "teacher" {
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "InstructorPageViewControllerNV")
                    }else{
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "MainSectionsViewControllerNV")
                    }
                }
            }
        }
        if lang == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage(lang)
            //LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .ar, rootViewController: initialViewController, animation: { view in
                //     MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            //
        } else if lang == "en" {
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lang)
            //  LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .en, rootViewController: initialViewController, animation: { view in
                //   MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
            
        }else{
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            Bundle.setLanguage(lang)
            //  LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .ku, rootViewController: initialViewController, animation: { view in
                //   MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
        }
    }

}
extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint(self)
        #endif
        return self
    }
}
