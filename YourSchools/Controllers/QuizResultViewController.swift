//
//  QuizResultViewController.swift
//  YourSchools
//
//  Created by iMac on 11/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage

class QuizResultViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource {
    var count = Int()
    var userExamsResults = ExamsResults(){
        didSet{
            self.QuizResultTableView.reloadData()
        }
    }
    @IBOutlet weak var QuizResultTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        QuizResultTableView.delegate = self
        QuizResultTableView.dataSource = self
        QuizResultTableView.separatorStyle = .none
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadExamsResultsForCurrentUser()
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return userExamsResults.data?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         count = userExamsResults.data?[section].results?.count ?? 0
        return count+3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row == 0 {
       cell = QuizResultTableView.dequeueReusableCell(withIdentifier: "QuizResultHeader", for: indexPath)
             let lbl = cell.viewWithTag(7) as! UILabel
            lbl.text = userExamsResults.data?[indexPath.section].exam
        }else if indexPath.row == 1 {
            cell = QuizResultTableView.dequeueReusableCell(withIdentifier: "QuizResultHeader1", for: indexPath)
        }else if indexPath.row == count+2 {
            cell = QuizResultTableView.dequeueReusableCell(withIdentifier: "QuizResultFooter", for: indexPath)
            let gradeLbl = cell.viewWithTag(10) as! UILabel
            gradeLbl.text = "\(userExamsResults.data?[indexPath.section].total ?? 0)"
        }else{
        cell = QuizResultTableView.dequeueReusableCell(withIdentifier: "QuizResultCell", for: indexPath)
            let subjectLbl = cell.viewWithTag(3) as! UILabel
            subjectLbl.text = userExamsResults.data?[indexPath.section].results?[indexPath.row-2].subject?.name
            let gradeLbl = cell.viewWithTag(4) as! UILabel
            gradeLbl.text = "Grade: \(userExamsResults.data?[indexPath.section].results?[indexPath.row-2].mark ?? 0)"
        }
        return cell
    }
    func loadExamsResultsForCurrentUser(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.ExamsResults() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.userExamsResults = try JSONDecoder().decode(ExamsResults.self, from: response.data!)
                        print(self.userExamsResults)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
