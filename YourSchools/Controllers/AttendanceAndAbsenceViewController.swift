//
//  AttendanceAndAbsenceViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

class AttendanceAndAbsenceViewController:  CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    var weekAttendance = Attendance(){
        didSet{
            self.tableView.reloadData()
        }
    }
     var nameColor =   [#colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 0.8210081336),#colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 0.8245933219),#colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.7069630623, green: 0.6857301593, blue: 0.4331979752, alpha: 1),#colorLiteral(red: 0.4282038808, green: 0.7693952918, blue: 0.973567903, alpha: 1),#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)]
    @IBOutlet weak var dateTextField: DesignableTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePiker: DesignableTextField!
    let datePicker = UIDatePicker()
    override func viewDidLoad() {
    super.viewDidLoad()
    tableView.separatorStyle = .none
    tableView.delegate = self
    tableView.dataSource = self
    datePicker.backgroundColor = .white
         showDatePicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        loadAttendanceData()
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    override func viewDidLayoutSubviews() {
        self.loopThroughSubViewAndAlignTextfieldText(subviews: [dateTextField])
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = weekAttendance.data?.count ?? 0
    return count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    if indexPath.row == 0 {
    let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceAndAbsenceHeader", for: indexPath)
        let lbl = cell.viewWithTag(7) as! UILabel
        let count = weekAttendance.data?.count ?? 0
        if count > 1 {
        lbl.text = NSLocalizedString("Last 6 Days", comment: "")
        }else{
            if let date = weekAttendance.data?.first?.date {
                 lbl.text = date
            }
        }

    return cell
    }else{
    let cell2 = tableView.dequeueReusableCell(withIdentifier: "AttendanceAndAbsenceCell", for: indexPath)
        let btn1 = cell2.viewWithTag(1) as! UIButton
        let btn2 = cell2.viewWithTag(2) as! UIButton
        let btn3 = cell2.viewWithTag(3) as! UIButton
        btn1.setTitle(weekAttendance.data?[indexPath.row-1].day ?? ""
, for: .normal)
        btn2.setTitle(weekAttendance.data?[indexPath.row-1].date ?? ""
            , for: .normal)
        btn3.setTitle(weekAttendance.data?[indexPath.row-1].status_formated ?? ""
            , for: .normal)
        if weekAttendance.data?[indexPath.row-1].status_formated == "حضور"{
           btn3.setTitleColor(#colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), for: .normal)
        }else{
           btn3.setTitleColor(#colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), for: .normal)
        }
            btn1.backgroundColor = nameColor[indexPath.row-1]
    return cell2
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
    return 1
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        loadAttendanceDataForSpecificDay(date:formatter.string(from: datePicker.date))
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func loadAttendanceData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.Attendance() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekAttendance = try JSONDecoder().decode(Attendance.self, from: response.data!)
                        print(self.weekAttendance)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func loadAttendanceDataForSpecificDay(date:String){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AttendanceInSpecificDay(date: date) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekAttendance = try JSONDecoder().decode(Attendance.self, from: response.data!)
                        print(self.weekAttendance)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
