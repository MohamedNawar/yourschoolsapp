//
//  ChatFilterVC.swift
//  YourSchools
//
//  Created by iMac on 12/23/19.
//  Copyright © 2019 iMac. All rights reserved.
//


import UIKit
import Alamofire
import PKHUD
import FCAlertView
import LanguageManager_iOS
class ChatFilterVC: CustomBaseVC , UIPickerViewDelegate , UIPickerViewDataSource {
      var cameFromMyRooms = false
      var addLessonCategories = AddLessonCategories(){
          didSet{
              self.educationalLevelWords = self.addLessonCategories.data?.options ?? [Stage]()
          }
      }

      func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
      }
     
      
      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
          if pickerView == sectionsPicker {
              return sectionsWords.count
          }
          if pickerView == classroomPicker {
              return classroomWords.count
          }
          if pickerView == educationalLevelPicker{
              return educationalLevelWords.count
          }
          return 0
      }
      func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          if pickerView == sectionsPicker {
              return sectionsWords[row].name
          }
          if pickerView == classroomPicker {
              return classroomWords[row].name
          }
          if pickerView == educationalLevelPicker{
              return educationalLevelWords[row].name ?? ""
          }
          return ""
      }
      let educationalLevelPicker = UIPickerView()
      let classroomPicker = UIPickerView()
      let sectionsPicker = UIPickerView()
      var educationalLevelWords = [Stage](){
          didSet{
              educationalLevelPicker.reloadAllComponents()
          }
      }
      var selectedEducationalLevel = ""
      var classroomWords = [Grade](){
          didSet{
              classroomPicker.reloadAllComponents()
          }
      }
      var educationalLevelWordsIndex = Int(){
          didSet{
              self.classroomWords = self.addLessonCategories.data?.options?[educationalLevelWordsIndex].grades?.options ?? [Grade]()
          }
      }
      var classroomWordsIndex = Int(){
          didSet{
            
              self.sectionsWords = self.addLessonCategories.data?.options?[educationalLevelWordsIndex].grades?.options?[classroomWordsIndex].sections?.options ?? [Section]()
              
          }
      }
      var selectedClassroom = ""
      var sectionsWordsIndex = Int()
      var sectionsWords = [Section](){
          didSet{
              sectionsPicker.reloadAllComponents()
          }
      }
      var subjectsWordsIndex = Int()
      var selectedSection = ""
      
      @IBOutlet weak var sectionTextField: UITextField!
      @IBOutlet weak var classroomTxtField: UITextField!
      @IBOutlet weak var educationalLevelTxtField: UITextField!

      var count = 1
      override func viewDidLoad() {
          super.viewDidLoad()
          getAddLessonCategories();
          sectionTextFieldPicker()
          classroomTxtFieldPicker()
          educationalLevelTxtFieldPicker()
          pickerInitialize()
      }
      func pickerInitialize(){
          educationalLevelPicker.delegate = self
          educationalLevelPicker.dataSource = self
          educationalLevelPicker.backgroundColor = .white
          educationalLevelPicker.showsSelectionIndicator = true;
          classroomPicker.delegate = self
          classroomPicker.dataSource = self
          classroomPicker.backgroundColor = .white
          classroomPicker.showsSelectionIndicator = true
          sectionsPicker.delegate = self
          sectionsPicker.dataSource = self
          sectionsPicker.backgroundColor = .white
          sectionsPicker.showsSelectionIndicator = true
      }
      func sectionTextFieldPicker(){
          let toolbar = UIToolbar();
          toolbar.sizeToFit()
          let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneSectionPicker));
          let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
          let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
          toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
          toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
          toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          sectionTextField.inputView = sectionsPicker
          sectionTextField.inputAccessoryView = toolbar
      }
      @objc func doneSectionPicker(){
          if sectionTextField.text == "" {
              if sectionsWords.count > 0 {
                  selectedSection = "\(sectionsWords[0].id ?? 0)"
                  sectionTextField.text = sectionsWords[0].name
                  sectionsWordsIndex = sectionsWords[0].id ?? 0
              } else {
                  self.dismiss(animated: true, completion: nil)
              }
          }
          self.view.endEditing(true)
          
      }
      @objc func cancelButtonPicker(){
          self.view.endEditing(true)
      }
      func classroomTxtFieldPicker(){
          let toolbar = UIToolbar();
          toolbar.sizeToFit()
          let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneClassroomPicker));
          let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
          let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
          toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
          toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
          toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          classroomTxtField.inputView = classroomPicker
          classroomTxtField.inputAccessoryView = toolbar
      }
      @objc func doneClassroomPicker(){
          if classroomTxtField.text == "" {
              if classroomWords.count > 0 {
                  selectedClassroom = "\(classroomWords[0].id ?? 0)"
                  classroomTxtField.text = classroomWords[0].name
                  classroomWordsIndex = 0
              } else {
                  self.dismiss(animated: true, completion: nil)
              }
          }
          sectionTextField.text = ""
          sectionTextField.isEnabled =  true
          self.view.endEditing(true)
          
      }
      func educationalLevelTxtFieldPicker(){
          let toolbar = UIToolbar();
          toolbar.sizeToFit()
          let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneEducationalLevelPicker));
          let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
          let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
          toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
          toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
          toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          educationalLevelTxtField.inputView = educationalLevelPicker
          educationalLevelTxtField.inputAccessoryView = toolbar
      }
      @objc func doneEducationalLevelPicker(){
          if educationalLevelTxtField.text == "" {
              if educationalLevelWords.count > 0 {
                  selectedEducationalLevel = "\(educationalLevelWords[0].id ?? 0)"
                  educationalLevelTxtField.text = educationalLevelWords[0].name
                  educationalLevelWordsIndex = 0
              } else {
                  self.dismiss(animated: true, completion: nil)
              }
          }
          classroomTxtField.text = ""
          sectionTextField.text = ""
          classroomTxtField.isEnabled = true
          sectionTextField.isEnabled =  false
          self.view.endEditing(true)
          
      }
      func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          if pickerView == educationalLevelPicker {
              if educationalLevelWords.count > 0 {
                  educationalLevelTxtField.text =  educationalLevelWords[row].name
                  selectedEducationalLevel = "\(educationalLevelWords[row].id ?? 0)"
                  educationalLevelWordsIndex = row
              }  else{
                  self.dismiss(animated: true, completion: nil)
              }
          }
          if pickerView == classroomPicker {
              if classroomWords.count > 0 {
                  classroomTxtField.text =  classroomWords[row].name
                   selectedClassroom = "\(classroomWords[row].id ?? 0)"
                  classroomWordsIndex = row
              }else{
                  self.dismiss(animated: true, completion: nil)
              }
          }
     
          if pickerView == sectionsPicker {
              if sectionsWords.count > 0 {
                  sectionTextField.text =  sectionsWords[row].name
                  sectionsWordsIndex = sectionsWords[row].id ?? 0
                  selectedSection = "\(sectionsWords[row].id ?? 0)"
              }else{
                  self.dismiss(animated: true, completion: nil)
              }
          }
      }
      override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
    
          self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
          self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
      }


      private func getAddLessonCategories(){
          let header = APIs.Instance.getHeader()
          HUD.show(.progress, onView: self.view)
          Alamofire.request(APIs.Instance.addLessonCategories() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
              HUD.hide()
              switch(response.result) {
              case .success(let value):
                  print(value)
                  
                  print(value)
                  let temp = response.response?.statusCode ?? 400
                  if temp >= 300 {
                      
                      do {
                          let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                          self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                          print(err.errors)
                      }catch{
                          print("errorrrrelse")
                      }
                  }else{
                      do {
                          self.addLessonCategories = try JSONDecoder().decode(AddLessonCategories.self, from: response.data!)
                      }catch{
                          print(error)
                          HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                      }
                  }
              case .failure(_):
                  HUD.hide()
                  let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                  HUD.flash(.label(lockString), delay: 1.0)
                  break
              }
          }
      }
      func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
          let alert = FCAlertView()
          alert.avoidCustomImageTint = true
          let updatedFrame = alert.bounds
          alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
          alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
      }
    
    @IBAction func filter(_ sender: Any) {
       print(selectedSection , selectedClassroom , selectedEducationalLevel)
        let filterDict:[String: String] = ["section_id" : "\(selectedSection)", "stage_id" : "\(selectedEducationalLevel)", "grade_id" : "\(selectedClassroom)"]
        
        // Post a notification
        if cameFromMyRooms{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "filterRooms"), object: nil, userInfo: filterDict)
        }else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "filterOthers"), object: nil, userInfo: filterDict)
        }
        self.navigationController?.popViewController(animated: true)
    }
}

