//
//  imagePresenterViewController.swift
//  YourSchools
//
//  Created by Mohamed Nawar on 1/11/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SDWebImage
import Auk
import moa
class imagePresenterViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var backGround: UIView!
    var images = [Image]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer.numberOfTapsRequired = 1
        self.backGround.addGestureRecognizer(mytapGestureRecognizer)
        scrollView.auk.settings.contentMode = .scaleToFill
        setScrollView(images: images)
    }
    func setScrollView(images:[Image]){
            for image in images {
                scrollView.auk.show(url:(image.url) ?? "")
            }
    }
    @objc func myTapAction(recognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        
    }
}
