//

//  LessonDataViewController.swift
//  YourSchools
//
//  Created by iMac on 11/7/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class LessonDataViewController:UIViewController , UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1992990154)
        self.navigationController?.navigationBar.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2025631421);
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return  4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 1
        }else if section == 2 {
            return 1
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UICollectionViewCell()
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LessonExplainationCell", for: indexPath)
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecordCell", for: indexPath)
            return cell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadCell", for: indexPath)
            return cell        }
    }
    
}
