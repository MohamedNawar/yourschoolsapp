//
//  ForgetPasswordViewController3.swift
//  YourSchools
//
//  Created by iMac on 9/23/19.
//  Copyright © 2019 iMac. All rights reserved.
//



    import UIKit
    import PKHUD
    import Alamofire
    import FCAlertView
    class ForgetPasswordViewController3: CustomBaseVC {
        @IBOutlet weak var passwordTextField : UITextField!
        var token = String()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
        }
        
        @IBAction func sendButtonPresed ( _ sender : Any) {
            
            forgetPasswordStep3()
        }
        
        private func forgetPasswordStep3(){
            if self.passwordTextField.text == ""  {
                HUD.flash(.label(NSLocalizedString("Enter your Password", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                return
            }
            
            let header = APIs.Instance.getHeader()
            
            let par = ["password": passwordTextField.text!,"password_confirmation": passwordTextField.text!,"token": token] as [String : String]
            print(par)
            
            HUD.show(.progress)
            Alamofire.request(APIs.Instance.resetPassword() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: NSLocalizedString("Error", comment: ""), SubTitle: NSLocalizedString("there is no account with the given phone number", comment: ""), Image:  #imageLiteral(resourceName: "Untitled-11"))
                            print(err.message)

                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            
                            print("successsss")
                            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            rootviewcontroller.rootViewController =   storyboard.instantiateViewController(withIdentifier: "LoginViewControllerNV")
                            rootviewcontroller.makeKeyAndVisible()
                        }catch{
                            HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                        }
                        HUD.flash(.success, delay: 1.0)
                        
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
        }
}
