//
//  ShowAllAvilableUnConnectedPeopleVC.swift
//  YourSchools
//
//  Created by iMac on 12/23/19.
//  Copyright © 2019 iMac. All rights reserved.
//


import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SDWebImage
class ShowAllAvilableUnConnectedPeopleVC: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    var tag = Int()
    var allTeachers = AllTeachers(){
        didSet{
            tableView.reloadData()
        }
    }

    @IBAction func filter(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatFilterVC") as! ChatFilterVC
           navVC.cameFromMyRooms = false
           self.navigationController?.pushViewController(navVC, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        getAllpersons()
        NotificationCenter.default.addObserver(self, selector: #selector(showFilteredData(notification:)), name: NSNotification.Name(rawValue: "filterOthers"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser(); self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
    }
    @objc func showFilteredData(notification: NSNotification) {
        getFilteredData(stageId: notification.userInfo?["stage_id"] as? String ?? "", grade_id: notification.userInfo?["grade_id"] as? String ?? "", section_id: notification.userInfo?["section_id"] as? String ?? "")
    }
    @IBOutlet weak var viewww: UIView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTeachers.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactSelectedTeacherViewController") as! ContactSelectedTeacherViewController
        navVC.id = allTeachers.data?[indexPath.row].id ?? 0
        navVC.selectedUserName = allTeachers.data?[indexPath.row].name ?? ""
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTeacherCell", for: indexPath)
        let lbl1 = cell.viewWithTag(1) as! UILabel
        let lbl2 = cell.viewWithTag(2) as! UILabel
        let lbl3 = cell.viewWithTag(3) as! UILabel
        let imageView = cell.viewWithTag(4) as! UIImageView
        let view = cell.viewWithTag(10) as! UIView
        let view1 = cell.viewWithTag(121) as! UIView
        if let teachersData = allTeachers.data {
            if let count = teachersData[indexPath.row].unseen_coversations_count {
                if count > 0 {
                    view1.isHidden = false
                }
            }
            lbl1.text = teachersData[indexPath.row].name ?? ""
            if let type = userData.Instance.data?.type {
                let data =  teachersData[indexPath.row]
                if type == "teacher" {
             lbl2.text = "\(data.section?.name ?? "") - \(data.section?.grade?.name ?? "") - \(data.section?.grade?.stage?.name ?? "")"
                }else{
                    lbl2.text = data.about ?? ""
                }
            }
            if teachersData[indexPath.row].is_connected == true {
                view.isHidden = false
            }else{
                view.isHidden = true
            }
                    if let  imageIndex = allTeachers.data?[indexPath.row].avatar {
                        imageView.sd_setImage(with: URL(string: imageIndex), placeholderImage: #imageLiteral(resourceName: "img34"))
                    }
        }
        return cell
    }
    private func getAllpersons(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        var request = String()
        if tag == 1 {
            request = APIs.Instance.getStudentWithoutRoom()
        }else{
            request = APIs.Instance.getTeachers()
        }
        print(request)
        Alamofire.request(request, method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.allTeachers = try JSONDecoder().decode(AllTeachers.self, from: response.data!)
                        if self.allTeachers.data?.count ?? 0 == 0 {
                            self.viewww.isHidden = false
                        }else{
                            self.viewww.isHidden = true
                        }
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    private func getFilteredData(stageId:String,grade_id:String,section_id:String){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        var request = APIs.Instance.getFilteredDataFromUserWithNoRoom(stageId:stageId,grade_id:grade_id,section_id:section_id)
        
        print(request)
        Alamofire.request(request, method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.allTeachers = try JSONDecoder().decode(AllTeachers.self, from: response.data!)
                        if self.allTeachers.data?.count ?? 0 == 0 {
                            self.viewww.isHidden = false
                        }else{
                            self.viewww.isHidden = true
                        }
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
