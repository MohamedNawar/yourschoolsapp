//
//  ForgetPasswordViewController.swift
//  YourSchools
//
//  Created by iMac on 9/23/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView


class ForgetPasswordViewController:CustomBaseVC {
    @IBOutlet weak var emailTxtField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2635699213, green: 0.6576808095, blue: 0.9320371747, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor =  #colorLiteral(red: 0.2635699213, green: 0.6576808095, blue: 0.9320371747, alpha: 1);
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTxtField.addPadding(UITextField.PaddingSide.left(20))
    }
    
    @IBAction func send(_ sender: Any) {
        userLogin()
    }
    private func userLogin(){
        
        let par = ["email":  "\(self.emailTxtField.text!)"] as [String : AnyObject]
        let header = APIs.Instance.getHeader()
        
        print(par )
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.resetPasswordVerifiy() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print (response)
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: NSLocalizedString("Error", comment: ""), SubTitle: NSLocalizedString("there is no account with the given phone number", comment: ""), Image:  #imageLiteral(resourceName: "Untitled-11"))
                        print(err.message)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController2") as! ForgetPasswordViewController2
                        navVC.mailText = "\(self.emailTxtField.text!)"
                        self.navigationController?.pushViewController(navVC, animated: true)
                        print ("successsserrorrrrelse")
                        
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
