//
//  SchoolNewsViewController.swift
//  YourSchools
//
//  Created by iMac on 11/5/18.
//  Copyright © 2018 iMac. All rights reserved.


import UIKit
import Alamofire
import SDWebImage
import FCAlertView
import PKHUD
import LanguageManager_iOS
class SchoolNewsViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    var Color =   [#colorLiteral(red: 0.4220864773, green: 0.452195406, blue: 0.58906883, alpha: 1),#colorLiteral(red: 0.9289425015, green: 0.566274941, blue: 0.2606683373, alpha: 1),#colorLiteral(red: 0.7069630623, green: 0.6857301593, blue: 0.4331979752, alpha: 1),#colorLiteral(red: 0.9829441905, green: 0.6422890425, blue: 0.7892381549, alpha: 1),#colorLiteral(red: 0.6688814759, green: 0.581382513, blue: 0.4969726205, alpha: 1),#colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 0.5262735445),#colorLiteral(red: 0.4282038808, green: 0.7693952918, blue: 0.973567903, alpha: 1),#colorLiteral(red: 0.5512533784, green: 0.7528917193, blue: 0.5331814885, alpha: 1),#colorLiteral(red: 0.9846814275, green: 0.5291161537, blue: 0.5994984508, alpha: 1),#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
    var news = News(){
        didSet{
            tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        loadSchoolNews();
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolNewsCell", for: indexPath) as! NewsCell
        cell.shareCallBack = {
            let myWebsite = NSURL(string: self.news.data?[indexPath.row].body ?? "")
            let shareAll = [ myWebsite]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)

        }
        let lbl1 = cell.viewWithTag(1) as! UIButton
        let view = cell.viewWithTag(10) as! UIView
        let image = cell.viewWithTag(2) as! UIImageView
        let lbl3 = cell.viewWithTag(3) as! UILabel
        let lbl4 = cell.viewWithTag(4) as! UILabel
        let lbl5 = cell.viewWithTag(5) as! UILabel
        let lbl6 = cell.viewWithTag(6) as! UILabel
        let lbl13 = cell.viewWithTag(13) as! UILabel
        if let newsData = news.data?[indexPath.row] {
            if newsData.is_urgent == 1 {
                lbl1.isHidden = false
            }else{
                lbl1.isHidden = true
            }
            image.sd_setImage(with: URL(string: newsData.avatar ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))
            lbl3.text = newsData.title ?? ""
            lbl4.text = newsData.body ?? ""
            lbl13.text = newsData.created_at ?? ""
            lbl5.text = "  \(NSLocalizedString("Watch", comment: ""))\(newsData.views_count ?? 0)"
            if LanguageManager.shared.currentLanguage.rawValue == "ar" {
                lbl5.textAlignment = .right
                lbl6.textAlignment = .right
                lbl3.textAlignment = .right
                lbl4.textAlignment = .right
            }
            view.backgroundColor = Color[indexPath.row%9]
            

        }

        return cell
    }
    private func loadSchoolNews(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.getSchoolNews())
    Alamofire.request(APIs.Instance.getSchoolNews() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.news = try JSONDecoder().decode(News.self, from: response.data!)
                        print(self.news)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "CurrentNewsViewController") as! CurrentNewsViewController
        navVC.currentNews = news.data?[indexPath.row] ?? NewsData()
        self.navigationController?.pushViewController(navVC, animated: true)

    }
}
