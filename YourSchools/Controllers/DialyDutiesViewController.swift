//
//  DialyDutiesViewController.swift
//  YourSchools
//
//  Created by iMac on 11/5/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage



class DialyDutiesViewController: CustomBaseVC , UICollectionViewDataSource , UICollectionViewDelegate {
    var section = Int()
    var subjects = Subjects(){
        didSet{
            CategoryCollectionView.reloadData()
        }
    }
    var currentUser = CurrentUser(){
        didSet{
            CategoryCollectionView.reloadData()
        }
    }
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser(); self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        showstudent(id:userData.Instance.data?.id ?? 0)
        showSubject()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subjects.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DialyDutiesCell", for: indexPath) as! MainCollectionViewCell1
        let btn = cell.viewWithTag(100) as! UIButton
        btn.isHidden = true
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
      
        image.sd_setImage(with: URL(string: subjects.data?[indexPath.row].avatar ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))
        lbl.text = subjects.data?[indexPath.row].name
        if let count = subjects.data?[indexPath.row].newly_lessons_count{
            if count > 0 {
                btn.isHidden = false
                btn.setTitle("\(count)", for: .normal)
            }
        }
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let modelName = UIDevice.modelName
        if screenWidth > 1000 {
            cell.bottomSpace.constant = 60.0
            lbl.font = UIFont(name:"Neo Sans W23 Regular", size: 26.0)
        }else{
            cell.bottomSpace.constant = 30.0
            lbl.font = UIFont(name:"Neo Sans W23 Regular", size: 18.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var sectionHeaderView = UICollectionReusableView()
        if collectionView == self.CategoryCollectionView {
            switch kind {
                
            case UICollectionView.elementKindSectionHeader:
                sectionHeaderView = CategoryCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DialyDutiesHeader", for: indexPath)
              
                let type = sectionHeaderView.viewWithTag(1) as! UILabel
                type.text = userData.Instance.data?.name ?? ""
                let type1 = sectionHeaderView.viewWithTag(2) as! UILabel
                type1.text = currentUser.data?.section?.grade?.stage?.name ?? ""
                section = currentUser.data?.section?.id ?? 0
           
                return sectionHeaderView
               
                
            default:
                assert(false, "Unexpected element kind")
            }
            
        }
        
        
        return sectionHeaderView
        
    }
    private func showstudent(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showCurrentStudent(id: id))
        Alamofire.request(APIs.Instance.showCurrentStudent(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.currentUser = try JSONDecoder().decode(CurrentUser.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    private func showSubject(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.getSubjects())
        Alamofire.request(APIs.Instance.getSubjects() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.subjects = try JSONDecoder().decode(Subjects.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentSubjectId = subjects.data?[indexPath.row].id
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "DailyAssignmentViewController") as! DailyAssignmentViewController
        navVC.title = subjects.data?[indexPath.row].name ?? ""
        navVC.section = section
        navVC.currentSubjectId = currentSubjectId ?? 0 ;
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
}
extension DialyDutiesViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.CategoryCollectionView.frame.width / 2) , height: (self.CategoryCollectionView.frame.width / 2))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}





