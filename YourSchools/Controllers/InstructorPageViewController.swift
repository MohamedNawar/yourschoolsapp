//
//  InstructorPageViewController.swift
//  YourSchools
//
//  Created by iMac on 11/7/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage
import FCAlertView
import RSSelectionMenu
import LanguageManager_iOS
import PKHUD
import Alamofire
import EasyNotificationBadge
class InstructorPageViewController: CustomBaseVC {
    var flag = Flag(){
          didSet{
            if self.flag.data?.unseen_rooms_count ?? 0 > 0 {
                tt1.setTitle("\(self.flag.data?.unseen_rooms_count ?? 0)", for: .normal)
                tt1.isHidden = false
            }else{
                tt1.isHidden = true
            }
            if self.flag.data?.unseen_adminstration_conversations_count ?? 0 > 0 {
                          tt.setTitle("\(self.flag.data?.unseen_adminstration_conversations_count ?? 0)", for: .normal)
                          tt.isHidden = false
                      }else{
                          tt.isHidden = true
                      }
          }
      }
    var notifications = Notifications()
    @IBOutlet weak var notificationBtn: UIBarButtonItem!
    @IBAction func goToNotifications(_ sender: Any) {
           let navVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
           self.navigationController?.pushViewController(navVC, animated: true)
       }
    @IBOutlet weak var tt1: UIButton!
    @IBOutlet weak var tt: UIButton!
    @IBOutlet weak var aboutTeacher: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var teacherName: UILabel!
    @IBOutlet weak var teacherImageView: UIImageView!
    @objc func showBadge() {
        
                if UserDefaults.standard.integer(forKey: "unReadNotify") != 0 &&  UserDefaults.standard.integer(forKey: "unReadNotify") != nil {
                    self.notificationBtn.badge(text: "\(UserDefaults.standard.integer(forKey: "unReadNotify"))")
                }else {
                    self.notificationBtn.badge(text: nil)
                }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showBadge),
                                               name: NSNotification.Name("NotificationBadge"),
                                               object: nil)

    }
    @IBAction func shoolTimetable(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
                     navVC.title = NSLocalizedString("School Schedule", comment: "")
                     navVC.tag = 0
                     self.navigationController?.pushViewController(navVC, animated: true)

    }
    @IBOutlet weak var langBtn: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showBadge()
        tt1.isHidden = true
        tt.isHidden = true
        showFlags()
        if LanguageManager.shared.currentLanguage.rawValue == "ar" {
            langBtn.setTitle("اللغة", for: .normal)
        } else if LanguageManager.shared.currentLanguage.rawValue == "en"{
            langBtn.setTitle("lan", for: .normal)
        }else{
            langBtn.setTitle("Ku", for: .normal)
        }
        userData.Instance.fetchUser()
        type.text = userData.Instance.data?.type ?? ""
        teacherName.text = userData.Instance.data?.name ?? ""
        aboutTeacher.text = userData.Instance.data?.about ?? ""
        if let imageIndex = userData.Instance.data?.avatar {
            teacherImageView.sd_setImage(with: URL(string: imageIndex), placeholderImage: #imageLiteral(resourceName: "img34"))
        };self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    
    @IBAction func slideMenu(_ sender: Any) {
        makeSignOutAlert(title: "", SubTitle: "Do you want to sign out?".localized(), Image: UIImage(named:"group1") ?? UIImage())
    }
    
    @IBAction func addLesson(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "AddLessonViewController") as! AddLessonViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func contactMangement(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactManagementViewController") as! ContactManagementViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func myProfile(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "TeacherUpdateProfileViewController") as! TeacherUpdateProfileViewController
        self.navigationController?.pushViewController(navVC, animated: true)
        
    }
    func makeSignOutAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.addButton("sign out".localized()) {
            userData.Instance.remove()
            userData.Instance.remove()
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "cancel".localized(), andButtons:nil)
    }
    private func showFlags(){
            let header = APIs.Instance.getHeader()
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.getFlags(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                print(response)
                switch(response.result) {
                case .success(let value):
                    HUD.hide()
                    let temp = response.response?.statusCode ?? 400
                    print(temp)
                    if temp >= 300 {
                        print("errorrrr")
                        do {
    //                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
    //                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image:#imageLiteral(resourceName: "Untitled-11") )
    //                        print(err.errors)
                        }catch{
                            print("errorrrrelse")
                            
                        }
                    }else{
                        
                        do {
                            self.flag = try JSONDecoder().decode(Flag.self, from: response.data!)
                            print(self.flag.data?.news_count)
                            let badgeCount: Int = self.flag.data!.news_count ?? 0
                            let application = UIApplication.shared
                            application.registerForRemoteNotifications()
                            application.applicationIconBadgeNumber = badgeCount
    //                                                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
    //                                                self.dismiss(animated: true, completion: nil)
                        }catch{
                            HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
            
        }
    @IBAction func contactStudent(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "EmailTeacherViewController") as! EmailTeacherViewController
        navVC.tag = 1; self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    @IBAction func langButtSlected(_ sender: UIButton) {
        let simpleDataArray = ["عربي","English","Kurds"]
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            if indexPath.row == 0{
                cell.textLabel?.text = object
            }else{
                cell.textLabel?.text = object
            }
        }
        
        
        selectionMenu.onDismiss = {(text) in
            if text.first == "عربي"{
                self.changeLang(lang: "ar")
            }else if text.first == "English" {
                self.changeLang(lang: "en")
            }else{
                self.changeLang(lang: "ku")
            }
            
        }
        
        // show as PresentationStyle = Push
        //selectionMenu.show(style: .Alert(title: "", action: "Done", height: 300), from: self)
        selectionMenu.show(style: .Popover(sourceView: sender, size: CGSize(width: 250, height: 100)), from: self)
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
           let alert = FCAlertView()
           alert.avoidCustomImageTint = true
           let updatedFrame = alert.bounds
           alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
           alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
       }
    func changeLang(lang:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
        if userData.Instance.firstUse == "" || userData.Instance.firstUse == nil {
            UserDefaults.standard.set("false", forKey: "firstUse")
            initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
            
        }else{
            if userData.Instance.token == "" || userData.Instance.token == nil {
                initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerNV")
            }else{
                if let type = userData.Instance.data?.type {
                    if type == "teacher" {
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "InstructorPageViewControllerNV")
                    }else{
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "MainSectionsViewControllerNV")
                    }
                }
            }
        }
        if lang == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage(lang)
//LanguageManager.shared.setLanguage(language: .ar)
                        LanguageManager.shared.setLanguage(language: .ar, rootViewController: initialViewController, animation: { view in
                       //     MessagesManger.shared.reinit()
                            view.transform = CGAffineTransform(scaleX: 2, y: 2)
                            view.alpha = 0
                        })
            //
        } else if lang == "en" {
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lang)
          //  LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .en, rootViewController: initialViewController, animation: { view in
                //   MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
            
        }else{
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            Bundle.setLanguage(lang)
            //  LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .ku, rootViewController: initialViewController, animation: { view in
                //   MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
        }
    }
}
