//
//  TeacherUpdateProfileViewController.swift
//  YourSchools
//
//  Created by iMac on 11/15/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import FCAlertView
import Alamofire
class TeacherUpdateProfileViewController: CustomBaseVC {
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var teacherNameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var ChangePasswordTxtField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        teacherNameTxtField.text = userData.Instance.data?.name ?? ""
        emailTxtField.text = userData.Instance.data?.email ?? ""
        phoneTextField.text = userData.Instance.data?.mobile ?? ""

        passwordTxtField.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
 teacherNameTxtField.addPadding(UITextField.PaddingSide.left(20))
        emailTxtField.addPadding(UITextField.PaddingSide.left(20))
        ChangePasswordTxtField.addPadding(UITextField.PaddingSide.left(20))
    }
    
    func PostProfileInfo(){
        if  self.emailTxtField.text == "" ||  self.teacherNameTxtField.text == ""  ||  self.phoneTextField.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter your Data", comment: "")), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        print(header)
        var par = [String : Any]()
        if passwordTxtField.text == "" && ChangePasswordTxtField.text == "" {
         par = ["name": teacherNameTxtField.text!, "email": emailTxtField.text! ,"mobile": phoneTextField.text!] as [String : Any]
        }else{
              par = ["name": teacherNameTxtField.text!, "email": emailTxtField.text! ,"mobile": phoneTextField.text! , "password": ChangePasswordTxtField.text!, "old_password": passwordTxtField.text!] as [String : Any]
        }
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.UserUpdateprofile(), method: .put, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("successsss")
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                        self.navigationController?.popViewController(animated: true)

                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }

    @IBAction func saveEditing(_ sender: Any) {
        PostProfileInfo()
    }
    @IBAction func dismiss(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLayoutSubviews() {
        self.loopThroughSubViewAndAlignTextfieldText(subviews: self.view.subviews)
    }
}
