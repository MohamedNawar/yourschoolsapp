//
//  MainSectionsViewController.swift
//  YourSchools
//
//  Created by iMac on 11/5/18.
//  Copyright © 2018 iMac. All rights reserved.
//
import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import SideMenu
import RSSelectionMenu
import LanguageManager_iOS
import UserNotifications

class MainSectionsViewController: CustomBaseVC , UICollectionViewDataSource , UICollectionViewDelegate  {
    var flag = Flag(){
        didSet{
            CategoryCollectionView.reloadData()
        }
    }
    @IBOutlet weak var langBtn: UIButton!
    
    
    
    var notification_view = ""
    var remote_notification_data : RemoteNotificationData?
    
    func remote_notification_action(){
            switch notification_view {
                case "lesson":
               let navVC = storyboard?.instantiateViewController(withIdentifier: "DialyDutiesViewController") as! DialyDutiesViewController
                self.navigationController?.pushViewController(navVC, animated: true)
                
                
//               if let currntLessonData = lessons.data?[indexPath.section].lessons?[indexPath.row-1] {
//                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowLessonViewController") as! ShowLessonViewController
//                navVC.lessonContent = currntLessonData
//                self.navigationController?.pushViewController(navVC, animated: true)
//                }
                
                
            case "attendance":
                    let navVC = storyboard?.instantiateViewController(withIdentifier: "AttendanceAndAbsenceViewController") as! AttendanceAndAbsenceViewController
                    navVC.title = NSLocalizedString( "Attendance", comment: "")
                self.navigationController?.pushViewController(navVC, animated: true)

            case "schedule":
                let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
                navVC.title = NSLocalizedString("School Schedule", comment: "")
                navVC.tag = 0
                self.navigationController?.pushViewController(navVC, animated: true)
                
            case "exams":
                let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
                navVC.title = NSLocalizedString("Exams Table", comment: "")
                navVC.tag = 1
                self.navigationController?.pushViewController(navVC, animated: true)
                
            case "result":
                let navVC = storyboard?.instantiateViewController(withIdentifier: "QuizResultViewController") as! QuizResultViewController
                navVC.title = NSLocalizedString("Exams Results", comment: "")
                self.navigationController?.pushViewController(navVC, animated: true)
                

 
            case "adminstration":
                let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactManagementViewController") as! ContactManagementViewController
                navVC.title =  NSLocalizedString("Contact Management", comment: "")
                self.navigationController?.pushViewController(navVC, animated: true)

            case "conversation":
//

                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactSelectedTeacherViewController") as! ContactSelectedTeacherViewController
                navVC.title =  NSLocalizedString("Teacher Conversation", comment: "")
                navVC.teacherFlag = "teacher"
                navVC.selectedUserName = remote_notification_data?.name ?? ""
                navVC.id = remote_notification_data?.id ?? 0
                self.navigationController?.pushViewController(navVC, animated: true)
                
  
            case "news":
                let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolNewsViewController") as! SchoolNewsViewController
                navVC.title = NSLocalizedString("School News", comment: "")
                self.navigationController?.pushViewController(navVC, animated: true)

            case "school_media" :
                let navVC = storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as! ImageCollectionViewController
                navVC.title = NSLocalizedString("Photo Gallery", comment: "")
                self.navigationController?.pushViewController(navVC, animated: true)
                
                
                default:
                break
            }
        }
 @IBOutlet weak var notificationBtn: UIBarButtonItem!
    @IBAction func goToNotifications(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    var currentUser = CurrentUser(){
        didSet{
            CategoryCollectionView.reloadData()
        }
    }
    var nameColor =   [#colorLiteral(red: 0.4220864773, green: 0.452195406, blue: 0.58906883, alpha: 1),#colorLiteral(red: 0.9289425015, green: 0.566274941, blue: 0.2606683373, alpha: 1),#colorLiteral(red: 0.4862077236, green: 0.8091998696, blue: 0.79018718, alpha: 1),#colorLiteral(red: 0.6546185613, green: 0.6628188491, blue: 0.662755847, alpha: 1),#colorLiteral(red: 0.7069630623, green: 0.6857301593, blue: 0.4331979752, alpha: 1),#colorLiteral(red: 0.9829441905, green: 0.6422890425, blue: 0.7892381549, alpha: 1),#colorLiteral(red: 0.6688814759, green: 0.581382513, blue: 0.4969726205, alpha: 1),#colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 0.5262735445),#colorLiteral(red: 0.4282038808, green: 0.7693952918, blue: 0.973567903, alpha: 1),#colorLiteral(red: 0.5512533784, green: 0.7528917193, blue: 0.5331814885, alpha: 1),#colorLiteral(red: 0.6787017584, green: 0.4313169122, blue: 0.700019598, alpha: 1),#colorLiteral(red: 0.9846814275, green: 0.5291161537, blue: 0.5994984508, alpha: 1)]
    var imagesArray = [#imageLiteral(resourceName: "img45"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img47"),#imageLiteral(resourceName: "img46"),#imageLiteral(resourceName: "img49") ,#imageLiteral(resourceName: "img48"),#imageLiteral(resourceName: "img51") ,#imageLiteral(resourceName: "img50"),#imageLiteral(resourceName: "img53") ,#imageLiteral(resourceName: "img52"),#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img54")]
    var nameArray = [NSLocalizedString( "Attendance", comment: ""),
                     NSLocalizedString("Daily Homework", comment: ""),
                     NSLocalizedString("School Schedule", comment: ""),
                     NSLocalizedString("Exams Table", comment: ""),
                     NSLocalizedString("Annual Installments", comment: ""),
                     NSLocalizedString("Student Behavior", comment: ""),
                     NSLocalizedString("Contact Management", comment: ""),
                     NSLocalizedString("Teacher Conversation", comment: ""),
                     NSLocalizedString("Word Of the Director", comment: ""),
                     NSLocalizedString("School News", comment: ""),
                     NSLocalizedString("Exams Results", comment: ""),
                     NSLocalizedString("Photo Gallery", comment: "")]
       @objc func refreshFlag() {
        if UserDefaults.standard.integer(forKey: "unReadNotify") != 0 &&  UserDefaults.standard.integer(forKey: "unReadNotify") != nil {
            self.notificationBtn.badge(text: "\(UserDefaults.standard.integer(forKey: "unReadNotify"))")
        }else {
            self.notificationBtn.badge(text: nil)
        }

        showFlags()
    }
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
  NotificationCenter.default.addObserver(self,
                                                selector: #selector(refreshFlag),
                                                name: NSNotification.Name("NotificationBadge"),
                                                object: nil)
        userData.Instance.fetchUser()
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        showFlags()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showstudent(id:userData.Instance.data?.id ?? 0)
        if LanguageManager.shared.currentLanguage.rawValue == "ar" {
            langBtn.setTitle("اللغة", for: .normal)
        } else if LanguageManager.shared.currentLanguage.rawValue == "en"{
            langBtn.setTitle("lan", for: .normal)
        }else{
            langBtn.setTitle("Ziman", for: .normal)
        }

           NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationComming"), object: nil)
        nameArray = [NSLocalizedString( "Attendance", comment: ""),
                         NSLocalizedString("Daily Homework", comment: ""),
                         NSLocalizedString("School Schedule", comment: ""),
                         NSLocalizedString("Exams Table", comment: ""),
                         NSLocalizedString("Annual Installments", comment: ""),
                         NSLocalizedString("Student Behavior", comment: ""),
                         NSLocalizedString("Contact Management", comment: ""),
                         NSLocalizedString("Teacher Conversation", comment: ""),
                         NSLocalizedString("Word Of the Director", comment: ""),
                         NSLocalizedString("School News", comment: ""),
                         NSLocalizedString("Exams Results", comment: ""),
                         NSLocalizedString("Photo Gallery", comment: "")]
        userData.Instance.fetchUser()
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        //check is there remote notification
        remote_notification_action()
        notification_view = ""
        refreshFlag()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "AttendanceAndAbsenceViewController") as! AttendanceAndAbsenceViewController
            navVC.title = NSLocalizedString( "Attendance", comment: "")
            self.navigationController?.pushViewController(navVC, animated: true)
            
        case 1:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "DialyDutiesViewController") as! DialyDutiesViewController
            navVC.title = NSLocalizedString("Daily Homework", comment: "")
 self.navigationController?.pushViewController(navVC, animated: true)
            
        case 2:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
            navVC.title = NSLocalizedString("School Schedule", comment: "")

            navVC.tag = 0
            self.navigationController?.pushViewController(navVC, animated: true)
            
        case 3:
//            let navVC = storyboard?.instantiateViewController(withIdentifier: "ExamScheduleViewController") as! ExamScheduleViewController
//            self.navigationController?.pushViewController(navVC, animated: true)
            let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDaysViewController") as! SchoolDaysViewController
            navVC.tag = 1
              navVC.title = NSLocalizedString("Exams Table", comment: "")
            
 self.navigationController?.pushViewController(navVC, animated: true)

        case 4:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "AnnualInstallmentsViewController") as! AnnualInstallmentsViewController
            navVC.title = NSLocalizedString("Annual Installments", comment: "")
            
 self.navigationController?.pushViewController(navVC, animated: true)
            
        case 5:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "StudentBehaviourViewController") as! StudentBehaviourViewController
          navVC.title =  NSLocalizedString("Student Behavior", comment: "")
 self.navigationController?.pushViewController(navVC, animated: true)
            
        case 6:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactManagementViewController") as! ContactManagementViewController
             navVC.title =  NSLocalizedString("Contact Management", comment: "")

 self.navigationController?.pushViewController(navVC, animated: true)
            
        case 7:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "EmailTeacherViewController") as! EmailTeacherViewController
            navVC.tag = 0;
             navVC.title =  NSLocalizedString("Teacher Conversation", comment: "")
self.navigationController?.pushViewController(navVC, animated: true)
        case 8:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MangerWordViewController") as! MangerWordViewController
            navVC.title = NSLocalizedString("Word Of the Director", comment: "")
 self.navigationController?.pushViewController(navVC, animated: true)

        case 9:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "SchoolNewsViewController") as! SchoolNewsViewController
            navVC.title = NSLocalizedString("School News", comment: "")
 self.navigationController?.pushViewController(navVC, animated: true)
            
        case 10:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "QuizResultViewController") as! QuizResultViewController
            navVC.title = NSLocalizedString("Exams Results", comment: "")
 self.navigationController?.pushViewController(navVC, animated: true)
            
        case 11:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as! ImageCollectionViewController
            navVC.title = NSLocalizedString("Photo Gallery", comment: "")
 self.navigationController?.pushViewController(navVC, animated: true)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainSectionsCell", for: indexPath) as! MainCollectionViewCell
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
        let btn = cell.viewWithTag(100) as! UIButton
        btn.isHidden = true
        image.image = imagesArray[indexPath.row]
        lbl.text = nameArray[indexPath.row]
        lbl.textColor = nameColor[indexPath.row]
        if indexPath.row == 2{
                                       if let count = flag.data?.schedules_count {
                                           if count > 0 {
                                               btn.isHidden = false
                                               btn.setTitle("\(count)", for: .normal)
                                           }
                                       }
                                   }
        if indexPath.row == 11{
                                 if let count = flag.data?.school_media_count {
                                     if count > 0 {
                                         btn.isHidden = false
                                         btn.setTitle("\(count)", for: .normal)
                                     }
                                 }
                             }
        if indexPath.row == 5{
                            if let count = flag.data?.warning_count {
                                if count > 0 {
                                    btn.isHidden = false
                                    btn.setTitle("\(count)", for: .normal)
                                }
                            }
                        }
        if indexPath.row == 3{
                     if let count = flag.data?.exams_count {
                         if count > 0 {
                             btn.isHidden = false
                             btn.setTitle("\(count)", for: .normal)
                         }
                     }
                 }
        if indexPath.row == 10{
               if let count = flag.data?.result_count {
                   if count > 0 {
                       btn.isHidden = false
                       btn.setTitle("\(count)", for: .normal)
                   }
               }
           }
        if indexPath.row == 4{
            if let count = flag.data?.installment_count {
                if count > 0 {
                    btn.isHidden = false
                    btn.setTitle("\(count)", for: .normal)
                }
            }
        }
        if indexPath.row == 0{
                 if let count = flag.data?.attendance_count {
                     if count > 0 {
                         btn.isHidden = false
                         btn.setTitle("\(count)", for: .normal)
                     }
                 }
             }
        if indexPath.row == 1{
            if let count = flag.data?.lessons_count {
                if count > 0 {
                    btn.isHidden = false
                    btn.setTitle("\(count)", for: .normal)
                }
            }
        }
        if indexPath.row == 8{
                  if let count = flag.data?.admin_word_count {
                      if count > 0 {
                          btn.isHidden = false
                          btn.setTitle("\(count)", for: .normal)
                      }
                  }
              }
        if indexPath.row == 9{
            if let count = flag.data?.news_count {
                if count > 0 {
                    btn.isHidden = false
                    btn.setTitle("\(count)", for: .normal)
                }
            }
        }
        if indexPath.row == 7{
            if let count = flag.data?.unseen_rooms_count {
                if count > 0 {
                    btn.isHidden = false
                    btn.setTitle("\(count)", for: .normal)
                }
            }
        }
        if indexPath.row == 6{
            if let count = flag.data?.unseen_adminstration_conversations_count {
                if count > 0 {
                    btn.isHidden = false
                    btn.setTitle("\(count)", for: .normal)
                }
            }
        }
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let modelName = UIDevice.modelName
        if screenWidth > 1000 {
            cell.bottomSpace.constant = 60.0
            lbl.font = UIFont(name:"Neo Sans W23 Regular", size: 26.0)
        }else{
            cell.bottomSpace.constant = 30.0
            lbl.font = UIFont(name:"Neo Sans W23 Regular", size: 18.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var sectionHeaderView = UICollectionReusableView()
        if collectionView == self.CategoryCollectionView {
            switch kind {
                
            case UICollectionView.elementKindSectionHeader:
                sectionHeaderView = CategoryCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MainSectionsHeader", for: indexPath)
                let type = sectionHeaderView.viewWithTag(1) as! UILabel
                let school_name = sectionHeaderView.viewWithTag(4) as! UILabel
                let student_img = sectionHeaderView.viewWithTag(3) as! UIImageView
                
                type.text = "\(userData.Instance.data?.name ?? "")\n\(userData.Instance.data?.section?.name ?? "")\n\(userData.Instance.data?.section?.grade?.name ?? "")\n\(userData.Instance.data?.section?.grade?.stage?.name ?? "")"
                
                school_name.text = userData.Instance.data?.school?.name
               
                student_img.sd_setImage(with: URL(string: currentUser.data?.avatar ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))

                
                return sectionHeaderView
                
            default:
                assert(false, "Unexpected element kind")
            }
        }
        return sectionHeaderView
    }
    func makeSignOutAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.addButton("sign out".localized()) {
            userData.Instance.remove()
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "cancel".localized(), andButtons:nil)
    }
    @IBAction func menue(_ sender: Any) {
            makeSignOutAlert(title: "", SubTitle: "Do you want to sign out?".localized(), Image: UIImage(named:"group1") ?? UIImage())
    }
    @IBAction func showProfile(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "StudentBehaviourViewController") as! StudentBehaviourViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    private func showFlags(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.getFlags(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image:#imageLiteral(resourceName: "Untitled-11") )
//                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        self.flag = try JSONDecoder().decode(Flag.self, from: response.data!)
                        print(self.flag.data?.news_count)
                        let badgeCount: Int = self.flag.data!.news_count ?? 0
                        let application = UIApplication.shared
                        application.registerForRemoteNotifications()
                        application.applicationIconBadgeNumber = badgeCount
//                                                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
//                                                self.dismiss(animated: true, completion: nil)
                    }catch{
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    private func showstudent(id:Int){
        let header = APIs.Instance.getHeader()
 
        HUD.show(.progress, onView: self.view)
    print(APIs.Instance.showCurrentStudent(id: id))
        Alamofire.request(APIs.Instance.showCurrentStudent(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.currentUser = try JSONDecoder().decode(CurrentUser.self, from: response.data!)

                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
extension MainSectionsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.CategoryCollectionView.frame.width / 2)
        let height = width - (width/6)
        return CGSize(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    @IBAction func langButtSlected(_ sender: UIButton) {
        let simpleDataArray = ["عربي","English","Kurds"]
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            if indexPath.row == 0{
                cell.textLabel?.text = object
            }else{
                cell.textLabel?.text = object
            }
        }
        
        
        selectionMenu.onDismiss = {(text) in
            if text.first == "عربي"{
                self.changeLang(lang: "ar")
            }else if text.first == "English" {
                self.changeLang(lang: "en")
            }else{
                self.changeLang(lang: "ku")
            }
            
        }
        
        // show as PresentationStyle = Push
        //selectionMenu.show(style: .Alert(title: "", action: "Done", height: 300), from: self)
        selectionMenu.show(style: .Popover(sourceView: sender, size: CGSize(width: 250, height: 100)), from: self)
    }
    
    func changeLang(lang:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
        if userData.Instance.firstUse == "" || userData.Instance.firstUse == nil {
            UserDefaults.standard.set("false", forKey: "firstUse")
            initialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
            
        }else{
            if userData.Instance.token == "" || userData.Instance.token == nil {
                initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerNV")
            }else{
                if let type = userData.Instance.data?.type {
                    if type == "teacher" {
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "InstructorPageViewControllerNV")
                    }else{
                        initialViewController = storyboard.instantiateViewController(withIdentifier: "MainSectionsViewControllerNV")
                    }
                }
            }
        }
        if lang == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage(lang)
            //LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .ar, rootViewController: initialViewController, animation: { view in
                //     MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            //
        } else if lang == "en" {
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lang)
            //  LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .en, rootViewController: initialViewController, animation: { view in
                //   MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
            
        }else{
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            Bundle.setLanguage(lang)
            //  LanguageManager.shared.setLanguage(language: .ar)
            LanguageManager.shared.setLanguage(language: .ku, rootViewController: initialViewController, animation: { view in
                //   MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
        }
    }
}





public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
