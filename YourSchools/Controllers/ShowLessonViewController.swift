//
//  ShowLessonViewController.swift
//  YourSchools
//
//  Created by iMac on 12/19/18.
//  Copyright © 2018 iMac. All rights reserved.
//
import Foundation
import UIKit
import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import AVKit
import AVFoundation
import MediaPlayer
class ShowLessonViewController: CustomBaseVC , UICollectionViewDelegate ,AVAudioPlayerDelegate , UICollectionViewDataSource {
    var is_play:Bool = false
    var lessonContent = LessonContentData(){
        didSet{
            print(lessonContent)
        }
    }
    @IBOutlet weak var listenRecordLbl: UILabel!
    @IBOutlet weak var watchVideoLbl: UILabel!
    @IBOutlet weak var lessonNameLbl: UILabel!
    @IBOutlet weak var noImgeLbl: UILabel!
    @IBOutlet weak var fileLbl: UILabel!
    @IBOutlet weak var filesCollectionView: UICollectionView!
    @IBOutlet weak var openCloseBtn: UIButton!
    @IBOutlet weak var lessonDetailsLbl: UILabel!
    @IBOutlet weak var showImageCollectionView: UICollectionView!
    var isOpen = true
    @IBOutlet weak var fileHeight: NSLayoutConstraint!
    @IBAction func playPressed(_ sender: UIButton) {
        if let url = lessonContent.audio?.url {
            print(url)
            let videoURL = URL(string: url)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            HUD.flash(.label(NSLocalizedString("There is no record", comment: "")), delay: 1.0)
            return
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        showLessonForSubject(id: "\(self.lessonContent.id ?? 0)")
        if self.lessonContent.audio?.url  == nil {
            listenRecordLbl.text = NSLocalizedString("There is no record", comment: "")
        }
        if self.lessonContent.video?.url  == nil {
            watchVideoLbl.text = NSLocalizedString("There is no video", comment: "")
        }

        lessonDetailsLbl.text = lessonContent.description ?? ""
        lessonNameLbl.text = lessonContent.name ?? ""
        showImageCollectionView.delegate = self
        showImageCollectionView.dataSource = self
        filesCollectionView.delegate = self
        filesCollectionView.dataSource = self
        if lessonContent.attachments?.count ?? 0 <= 0 {
            filesCollectionView.borderWidth = 0
            fileHeight.constant = 0.0
            fileLbl.isHidden = true
        }else{
            let count = lessonContent.attachments?.count ?? 0
            fileHeight.constant = CGFloat(count*50)
        }
        if lessonContent.images?.count ?? 0 <= 0 {
            noImgeLbl.isHidden = false
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == filesCollectionView {
            return lessonContent.attachments?.count ?? 0
        }
        if collectionView == showImageCollectionView {
            let count = lessonContent.images?.count ?? 0
            print(count)
            return count
        }
        return 0
    }
    private func showLessonForSubject(id:String){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.getLessons(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        let lessonContent = try JSONDecoder().decode(LessonContentData.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if collectionView == filesCollectionView {
            cell = filesCollectionView.dequeueReusableCell(withReuseIdentifier: "ShowFileCell", for: indexPath)
            let name = cell.viewWithTag(10) as! UILabel
            name.text = "\(lessonContent.attachments?[indexPath.item].type ?? "")\(indexPath.item)"
            return cell
        }
        if collectionView == showImageCollectionView {
            print("echo")
            cell = showImageCollectionView.dequeueReusableCell(withReuseIdentifier: "ShowImageCell", for: indexPath)
            let img = cell.viewWithTag(32) as! UIImageView
            if let image = lessonContent.images?[indexPath.row].url{
                img.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "img34"))
            }
            return cell    }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == filesCollectionView {
            if let url = lessonContent.attachments?[indexPath.row].url {
                if let url = NSURL(string: url){
                    UIApplication.shared.openURL(url as URL)
                }
                
            }else{
                return
            }
        }
        if collectionView == showImageCollectionView {
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "imagePresenterViewController") as! imagePresenterViewController
            print(lessonContent.images?.first?.url ?? "")
            navVC.images = lessonContent.images ?? [Image]()
            navVC.modalPresentationStyle = .overFullScreen
            self.present(navVC, animated: true, completion: nil)
//            showLightbox()
        }
    }
//    @IBAction func showAndHideDetails(_ sender: Any) {
//        if isOpen == false {
////            openCloseBtn.setImage(#imageLiteral(resourceName: "8"), for: .normal)
//            lessonDetailsLbl.text = lessonContent.description ?? ""
//        }else{
////            openCloseBtn.setImage(#imageLiteral(resourceName: "7"), for: .normal)
//            lessonDetailsLbl.text = ""
//        }
//        isOpen = !isOpen
//    }
    
    @IBAction func playVideo(_ sender: Any) {
        
        if let url = lessonContent.video?.url {
            print(url)
            let videoURL = URL(string: url)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            HUD.flash(.label(NSLocalizedString("There is no video", comment: "")), delay: 1.0)
            return
        }
    }
}
extension ShowLessonViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == filesCollectionView {
            let width = (self.filesCollectionView.frame.width)
            let height = width - (width)
            return CGSize(width: width , height: 50)
        }
        if collectionView == showImageCollectionView {
            return CGSize(width: 160 , height: 160)
        }
        
        return CGSize()
    }
}
extension String
{
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
}
