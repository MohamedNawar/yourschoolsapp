//
//  UserProfileViewController.swift
//  YourSchools
//
//  Created by iMac on 11/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class UserProfileViewController:  UIViewController {
    @IBOutlet weak var schoolCode: UILabel!
    @IBOutlet weak var UserName: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
       @IBOutlet weak var phoneTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
           self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.clear]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        UserName.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0);
    }
    @IBAction func Save(_ sender: Any) {
        
    }
    override func viewDidLayoutSubviews() {
        self.loopThroughSubViewAndAlignTextfieldText(subviews: self.view.subviews)
    }
}



