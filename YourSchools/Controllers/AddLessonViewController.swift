
import UIKit
import Alamofire
import PKHUD
import FCAlertView
import YPImagePicker
import MediaPlayer
import AVFoundation
import MobileCoreServices
import MobileCoreServices
import LanguageManager_iOS
class AddLessonViewController: CustomBaseVC , UITextViewDelegate ,UICollectionViewDelegate , UICollectionViewDataSource ,  UINavigationControllerDelegate , MPMediaPickerControllerDelegate , UIPickerViewDelegate , UIPickerViewDataSource ,AVAudioRecorderDelegate, AVAudioPlayerDelegate ,UIDocumentPickerDelegate {
    var recordURL = ""
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let selectedFileURL = urls.first else {
            return
        }
        
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let sandboxFileURL = dir.appendingPathComponent(selectedFileURL.lastPathComponent)
        documentUrl.append(sandboxFileURL)
        if FileManager.default.fileExists(atPath: sandboxFileURL.path) {
            print("Already exists! Do nothing")
        }
        else {
            
            do {
                try FileManager.default.copyItem(at: selectedFileURL, to: sandboxFileURL)
                
                print("Copied file!")
            }
            catch {
                print("Error: \(error)")
            }
        }
    }
    
    func clickFunction(){
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypePlainText)], in: .import)
        documentPicker.delegate = self
        documentPicker.allowsMultipleSelection = false
        present(documentPicker, animated: true) {
            documentPicker.view.tintColor = #colorLiteral(red: 0.2619069219, green: 0.580401361, blue: 0.800809741, alpha: 1)
        }
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    var documentUrl = [URL](){
        didSet{
            filesCollectionView.reloadData()
        }
    }
    var audioPlayer : AVAudioPlayer!
    var recordingSession:AVAudioSession!
    var audioRecorder:AVAudioRecorder!
    var settings = [String : Int]()
    var totalTime = 0
    var didRecord = false
    var pressPlay = false
    var countdownTimer: Timer!
    var addLessonCategories = AddLessonCategories(){
        didSet{
            self.educationalLevelWords = self.addLessonCategories.data?.options ?? [Stage]()
        }
    }
    override func viewDidLayoutSubviews() {
        self.loopThroughSubViewAndAlignTextfieldText(subviews: self.view.subviews)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    @IBOutlet weak var title11: UILabel!
    @IBAction func removeRecord(_ sender: Any) {
        removeRecordBtn.isHidden = true
        recordingLbl.isHidden = true
        playBtn.isHidden = true
        countdownTimer.invalidate()
        durationLbl.text = NSLocalizedString("Send Voice Message", comment: "")
        totalTime = 0
        audioRecorder = nil
    }
    @IBOutlet weak var removeRecordBtn: UIButton!
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == sectionsPicker {
            return sectionsWords.count
        }
        if pickerView == classroomPicker {
            return classroomWords.count
        }
        if pickerView == subjectsPicker {
            return subjectsWords.count
        }
        if pickerView == educationalLevelPicker{
            return educationalLevelWords.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == sectionsPicker {
            return sectionsWords[row].name
        }
        if pickerView == classroomPicker {
            return classroomWords[row].name
        }
        if pickerView == subjectsPicker {
            return subjectsWords[row].name
        }
        if pickerView == educationalLevelPicker{
            return educationalLevelWords[row].name ?? ""
        }
        return ""
    }
    @IBOutlet weak var filesCollectionView: UICollectionView!
    var imagesAfterArray = [String]()
    var videoPicker = YPImagePicker()
    var imagePicker = YPImagePicker()
    let educationalLevelPicker = UIPickerView()
    let classroomPicker = UIPickerView()
    let subjectsPicker = UIPickerView()
    let sectionsPicker = UIPickerView()
    var educationalLevelWords = [Stage](){
        didSet{
            educationalLevelPicker.reloadAllComponents()
        }
    }
    var selectedEducationalLevel = ""
    var classroomWords = [Grade](){
        didSet{
            classroomPicker.reloadAllComponents()
        }
    }
    var educationalLevelWordsIndex = Int(){
        didSet{
            self.classroomWords = self.addLessonCategories.data?.options?[educationalLevelWordsIndex].grades?.options ?? [Grade]()
        }
    }
    var classroomWordsIndex = Int(){
        didSet{
            self.subjectsWords = self.addLessonCategories.data?.options?[educationalLevelWordsIndex].grades?.options?[classroomWordsIndex].subjects?.options ?? [SubjectData]()
            self.sectionsWords = self.addLessonCategories.data?.options?[educationalLevelWordsIndex].grades?.options?[classroomWordsIndex].sections?.options ?? [Section]()
            
        }
    }
    var selectedClassroom = ""
    var subjectsWords = [SubjectData](){
        didSet{
            subjectsPicker.reloadAllComponents()
        }
    }
    var sectionsWordsIndex = Int()
    var selectedSubject = ""
    var sectionsWords = [Section](){
        didSet{
            sectionsPicker.reloadAllComponents()
        }
    }
    var subjectsWordsIndex = Int()
    var selectedSection = ""
    @IBAction func playPressed(_ sender: Any) {
        if !audioRecorder.isRecording {
            removeRecordBtn.isHidden = false
            if pressPlay == false {
                self.audioPlayer = try! AVAudioPlayer(contentsOf: audioRecorder.url)
                print(audioRecorder.url)
                self.audioPlayer.prepareToPlay()
                self.audioPlayer.delegate = self
                audioPlayer.volume = 1
                do {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                } catch _ {
                }
                self.audioPlayer.play()
                pressPlay = true
                playBtn.setImage(#imageLiteral(resourceName: "pause (1)"), for: .normal)
            }else{
                self.audioPlayer.stop()
                pressPlay = false
                playBtn.setImage(#imageLiteral(resourceName: "play-button (1) (1)"), for: .normal)
            }
        }
    }
    @IBAction func startRecording(_ sender: Any) {
        removeRecordBtn.isHidden = true
        UIImpactFeedbackGenerator().impactOccurred()
        self.startRecording()
        recordingLbl.isHidden = false
        playBtn.isHidden = true
        startTimer()
    }
    @IBAction func stopRecording(_ sender: Any) {
        self.finishRecording(success: true)
        UINotificationFeedbackGenerator().notificationOccurred(.success)
        recordingLbl.isHidden = true
        playBtn.isHidden = false
        playBtn.isEnabled = true
        endTimer()
        removeRecordBtn.isHidden = false
        didRecord = true
        
    }
    
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var recordingLbl: UILabel!
    @IBOutlet var durationLbl: UILabel!
    @IBOutlet weak var sectionTextField: UITextField!
    @IBOutlet weak var subjectTxtField: UITextField!
    @IBOutlet weak var classroomTxtField: UITextField!
    @IBOutlet weak var educationalLevelTxtField: UITextField!
    @IBOutlet weak var videoPickerCollection: UICollectionView!
    @IBOutlet weak var LessonName: UITextField!
    @IBOutlet weak var LessonDetails: UITextView!
    @IBOutlet weak var LessonDetailsView: UIView!
    @IBOutlet weak var imagePickerCollection: UICollectionView!
    var imageArray = [YPMediaPhoto](){
        didSet{
            imagePickerCollection.reloadData()
        }
    }
    var videoArray = [YPMediaVideo](){
        didSet{
            videoPickerCollection.reloadData()
        }
    }
    var count = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        askForRecordingPremission()
        getAddLessonCategories();
        imageArray.removeAll()
        LessonDetails.text = "Explain Lesson Details".localized()
        LessonDetails.textColor = UIColor.lightGray
        LessonDetails.delegate = self as! UITextViewDelegate
        filesCollectionView.delegate = self
        filesCollectionView.dataSource = self
        imagePickerCollection.delegate = self
        imagePickerCollection.dataSource = self
        videoPickerCollection.delegate = self
        videoPickerCollection.dataSource = self
        sectionTextField.addPadding(UITextField.PaddingSide.left(20))
        subjectTxtField.addPadding(UITextField.PaddingSide.left(20))
        classroomTxtField.addPadding(UITextField.PaddingSide.left(20))
        educationalLevelTxtField.addPadding(UITextField.PaddingSide.left(20))
        LessonName.addPadding(UITextField.PaddingSide.left(20))
        sectionTextFieldPicker()
        subjectTxtFieldPicker()
        classroomTxtFieldPicker()
        educationalLevelTxtFieldPicker()
        configureImagePicker()
        configureVideoPicker()
        pickerInitialize()
    }
    func pickerInitialize(){
        educationalLevelPicker.delegate = self
        educationalLevelPicker.dataSource = self
        educationalLevelPicker.backgroundColor = .white
        educationalLevelPicker.showsSelectionIndicator = true;
        classroomPicker.delegate = self
        classroomPicker.dataSource = self
        classroomPicker.backgroundColor = .white
        classroomPicker.showsSelectionIndicator = true
        subjectsPicker.delegate = self
        subjectsPicker.dataSource = self
        subjectsPicker.backgroundColor = .white
        subjectsPicker.showsSelectionIndicator = true
        sectionsPicker.delegate = self
        sectionsPicker.dataSource = self
        sectionsPicker.backgroundColor = .white
        sectionsPicker.showsSelectionIndicator = true
    }
    func sectionTextFieldPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneSectionPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        sectionTextField.inputView = sectionsPicker
        sectionTextField.inputAccessoryView = toolbar
    }
    @objc func doneSectionPicker(){
        if sectionTextField.text == "" {
            if sectionsWords.count > 0 {
                sectionTextField.text = sectionsWords[0].name
                sectionsWordsIndex = sectionsWords[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func cancelButtonPicker(){
        self.view.endEditing(true)
    }
    func subjectTxtFieldPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneSubjectPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        subjectTxtField.inputView = subjectsPicker
        subjectTxtField.inputAccessoryView = toolbar
    }
    @objc func doneSubjectPicker(){
        if subjectTxtField.text == "" {
            if subjectsWords.count > 0 {
                subjectTxtField.text = subjectsWords[0].name
                subjectsWordsIndex = subjectsWords[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        self.view.endEditing(true)
        
    }
    func classroomTxtFieldPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneClassroomPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        classroomTxtField.inputView = classroomPicker
        classroomTxtField.inputAccessoryView = toolbar
    }
    @objc func doneClassroomPicker(){
        if classroomTxtField.text == "" {
            if classroomWords.count > 0 {
                classroomTxtField.text = classroomWords[0].name
                classroomWordsIndex = 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        sectionTextField.text = ""
        subjectTxtField.text = ""
        sectionTextField.isEnabled =  true
        subjectTxtField.isEnabled =  true
        self.view.endEditing(true)
        
    }
    func educationalLevelTxtFieldPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneEducationalLevelPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelButtonPicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        educationalLevelTxtField.inputView = educationalLevelPicker
        educationalLevelTxtField.inputAccessoryView = toolbar
    }
    @objc func doneEducationalLevelPicker(){
        if educationalLevelTxtField.text == "" {
            if educationalLevelWords.count > 0 {
                educationalLevelTxtField.text = educationalLevelWords[0].name
                educationalLevelWordsIndex = 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        classroomTxtField.text = ""
        sectionTextField.text = ""
        subjectTxtField.text = ""
        classroomTxtField.isEnabled = true
        sectionTextField.isEnabled =  false
        subjectTxtField.isEnabled =  false
        self.view.endEditing(true)
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == educationalLevelPicker {
            if educationalLevelWords.count > 0 {
                educationalLevelTxtField.text =  educationalLevelWords[row].name
                educationalLevelWordsIndex = row
            }  else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == classroomPicker {
            if classroomWords.count > 0 {
                classroomTxtField.text =  classroomWords[row].name
                classroomWordsIndex = row
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == subjectsPicker {
            if subjectsWords.count > 0 {
                subjectTxtField.text =  subjectsWords[row].name
                subjectsWordsIndex = subjectsWords[row].id ?? 0
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == sectionsPicker {
            if sectionsWords.count > 0 {
                sectionTextField.text =  sectionsWords[row].name
                sectionsWordsIndex = sectionsWords[row].id ?? 0
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if LanguageManager.shared.currentLanguage == .ar {
            title11.textAlignment = .right
        }else{
            title11.textAlignment = .left
        }
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    func configureImagePicker(){
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photo
        config.library.maxNumberOfItems = 10
        config.library.onlySquare  = true
        config.targetImageSize = .original
        config.startOnScreen = .library
        config.wordings.libraryTitle = "Gallery"
        config.hidesStatusBar = false
        config.isScrollToChangeModesEnabled = false
        imagePicker = YPImagePicker(configuration: config)
    }
    func configureVideoPicker(){
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .video
        config.library.onlySquare  = false
        config.onlySquareImagesFromCamera = true
        config.targetImageSize = .original
        config.usesFrontCamera = true
        config.showsFilters = true
        config.shouldSaveNewPicturesToAlbum = true
        config.video.compression = AVAssetExportPresetHighestQuality
        config.albumName = "MyGreatAppName"
        config.screens = [.library, .photo, .video]
        config.startOnScreen = .library
        config.video.recordingTimeLimit = 10
        config.showsCrop = .rectangle(ratio: (16/9))
        config.wordings.libraryTitle = "Videos"
        config.hidesStatusBar = false
        config.library.maxNumberOfItems = 5
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 3
        config.library.spacingBetweenItems = 2
        config.video.compression = AVAssetExportPreset640x480
        config.isScrollToChangeModesEnabled = false
        videoPicker = YPImagePicker(configuration: config)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        LessonDetailsView.borderWidth = 1
        textView.text = ""
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Explain Lesson Details".localized()
            textView.textColor = UIColor.lightGray
            LessonDetailsView.borderWidth = 1
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == imagePickerCollection {
            if section == 0 {
                return 1
            }else {
                if imageArray.count > 0 {
                    count = imageArray.count
                    return count
                }else{
                    return 0
                }
            }
        }
        if collectionView == filesCollectionView {
            if section == 0 {
                return 1
            }else {
                if documentUrl.count > 0 {
                    count = documentUrl.count
                    return count
                }else{
                    return 0
                }
            }
        }
        if collectionView == videoPickerCollection {
            if section == 0 {
                return 1
            }else {
                if videoArray.count > 0 {
                    count = videoArray.count
                    return count
                }else{
                    return 0
                }
            }
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if collectionView == imagePickerCollection {
            if indexPath.section == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectImageCell", for: indexPath)
                return cell
            }else{
                if indexPath.row < imageArray.count {
                    let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCollectionViewCell
                    
                    var image = cell2.viewWithTag(32) as! UIImageView
                    image.image = imageArray[indexPath.row].image
                    cell2.DeleteCallBack = {
                        self.imageArray.remove(at: indexPath.item)
                    }
                    return cell2
                }
            }
        }
        if collectionView == filesCollectionView {
            if indexPath.section == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addFileCell", for: indexPath)
                return cell
            }else{
                if indexPath.row < documentUrl.count {
                    let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowFileCell1", for: indexPath) as! fileCollectionViewCell
                    let name = cell2.viewWithTag(10) as! UILabel
                    name.text = documentUrl[indexPath.row].lastPathComponent
                    cell2.DeleteCallBack = {
                        self.documentUrl.remove(at: indexPath.item)
                    }
                    
                    return cell2
                }
            }
        }
        if collectionView == videoPickerCollection {
            if indexPath.section == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectVideoCell", for: indexPath)
                return cell
            }else{
                
                if indexPath.row < videoArray.count {
                    let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath) as! videoCollectionViewCell
                    var result = videoArray[indexPath.item]
                    var image = cell2.viewWithTag(32) as! UIImageView
                    image.image = result.thumbnail
                    
                    cell2.DeleteCallBack = {
                        self.videoArray.remove(at: indexPath.item)
                    }
                    return cell2
                }
            }
        }
        
        return cell
    }
    func audioPicker(){
        var mediaPicker : MPMediaPickerController?
        mediaPicker = MPMediaPickerController(mediaTypes: .music)
        if let picker = mediaPicker {
            picker.delegate = self
            picker.allowsPickingMultipleItems = false
            picker.showsCloudItems = false
            picker.prompt = "Please Pick a Song"
            present(picker, animated: false, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == filesCollectionView {
            if indexPath.section == 0 {
                clickFunction()
                present(imagePicker, animated: true, completion: nil)
            }
        }
        if collectionView == imagePickerCollection {
            if indexPath.section == 0 {
                configureImagePicker()
                imagePicker.didFinishPicking { [unowned imagePicker] items, _ in
                    for item in items {
                        switch item {
                        case .photo(let photo):
                            self.imageArray.append(photo)
                        case .video(let video):
                            print("v")
                        }
                    }
                    imagePicker.dismiss(animated: true, completion: nil)
                }
                present(imagePicker, animated: true, completion: nil)
            }
        }
        if collectionView == videoPickerCollection {
            if indexPath.section == 0 {
                configureVideoPicker()
                videoPicker.didFinishPicking { [unowned videoPicker] items, _ in
                    if let video = items.singleVideo {
                        self.videoArray.removeAll()
                        self.videoArray.append(video)
                    }
                    videoPicker.dismiss(animated: true, completion: nil)
                }
                present(videoPicker, animated: true, completion: nil)
            }
        }
    }
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        
        print("you picked: \(mediaItemCollection)")//This is the picked media item.
        //  If you allow picking multiple media, then mediaItemCollection.items will return array of picked media items(MPMediaItem)
    }
    private func getAddLessonCategories(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.addLessonCategories() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let value):
                print(value)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.addLessonCategories = try JSONDecoder().decode(AddLessonCategories.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    /////////
    func askForRecordingPremission(){
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        print(soundURL)
        return soundURL as NSURL?
    }
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    func finishRecording(success: Bool) {
        self.removeRecordBtn.isHidden = false
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
   
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    func endTimer() {
        countdownTimer.invalidate()
        totalTime = 0
    }
    @objc func updateTime() {
        totalTime += 1
        durationLbl.text = "\(timeFormatted(totalTime))"
        
    }
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
  
    func addLesson(){
        HUD.show(.progress)
        if self.sectionTextField.text == "" {
            HUD.flash(.label("Enter your Section"), delay: 1.0)
            return
        }
        if self.LessonName.text == "" {
            HUD.flash(.label("Enter your Lesson Name"), delay: 1.0)
            return
        }
        if self.LessonDetails.text == "" {
            HUD.flash(.label("Enter your Lesson Details"), delay: 1.0)
            return
        }
        if self.classroomTxtField.text == "" {
            HUD.flash(.label("Enter your Classroom"), delay: 1.0)
            return
        }
        if self.educationalLevelTxtField.text == "" {
            HUD.flash(.label("Enter your Educational Level"), delay: 1.0)
            return
        }
        if self.subjectTxtField.text == "" {
            HUD.flash(.label("Enter Subject Name"), delay: 1.0)
            return
        }
        
        let header = APIs.Instance.getHeader()
        ///////////
//        HUD.show(.progress, onView: self.view)
        var par = ["section_id": "\(sectionsWordsIndex)", "name": LessonName.text! , "description": LessonDetails.text!,"subject_id": "\(subjectsWordsIndex)" , ] as [String : Any]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in par {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if self.didRecord {
                print(self.audioRecorder.url)
                multipartFormData.append(self.audioRecorder.url, withName: "audio", fileName: "sound.mp3", mimeType: "audio/mp3")
             
                print(par)
            }
            if self.videoArray.count>0 {
                if let url = self.videoArray.first?.url{
                    
                    multipartFormData.append(url, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
                }
            }
            if self.imageArray.count > 0 {
                for (index, image) in self.imageArray.enumerated() {
                    multipartFormData.append(image.image.jpegData(compressionQuality: 0.1)!, withName: "images[]", fileName: "image\(index).jpeg", mimeType: "image/jpeg")
                }
                
            }
            if self.documentUrl.count > 0 {
                for (index, doc) in self.documentUrl.enumerated() {
                    multipartFormData.append(doc, withName: "attachments[]", fileName: "doc\(index).pdf", mimeType: "application/pdf")
                }
            }
            print("------\(multipartFormData)")
        }, usingThreshold: UInt64.init(), to: APIs.Instance.addLesson(), method: .post, headers: APIs.Instance.getHeader()) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                  
                    
                    if let _ = response.error{
                        
                        return
                    }
                    print("Succesfully uploaded----\(response)")
                    //onCompletion?(nil)
                    print(response.result.value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                            print(err.errors)
                            print(err.errors?.audio)
                            print(err.errors?.subject_id)
                            print(err.errors?.images)
                            HUD.hide()
                        }catch{
                            HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }else{
                        do {
                            HUD.hide()
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationDoneViewController")
                            self.present(vc!, animated: true, completion: nil)
                            HUD.hide()
                        }catch{
                            HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }
                    HUD.hide()
                }
            case .failure(let error):
                HUD.hide()
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    
    @IBAction func upload(_ sender: Any) {
        addLesson()
    }
}


extension AddLessonViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == filesCollectionView {
            let width = (self.filesCollectionView.frame.width)
            let height = width - (width - 10)
            return CGSize(width: width , height: 50)
        }else{
            return CGSize(width: 100 , height: 100)
        }
    }
    
}
