//
//  TutorialViewController.swift
//  YourSchools
//
//  Created by iMac on 11/8/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class TutorialViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipBtn: UIButton!
    
    var pages = [TutorialStepViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                    
        if LanguageManager.shared.currentLanguage.rawValue == "ar" {
         skipBtn.setTitle("تخطي", for: .normal)
        }
        if LanguageManager.shared.currentLanguage.rawValue == "ku" {
            skipBtn.setTitle("berdan", for: .normal)
        }
        scrollView.isDirectionalLockEnabled = true
        scrollView.isPagingEnabled = true
        let page1 = createAndAddTutorialStep("img02-1", iconImageName: "img32", text: NSLocalizedString("PetShare is a pet photo sharing community.", comment: ""))
        let page2 = createAndAddTutorialStep("img02-1", iconImageName: "img33", text: NSLocalizedString("Take picture of your pet, and add filters or clipart to help them shine.", comment: ""))
        let page3 = createAndAddTutorialStep("img02-1", iconImageName: "img26", text: NSLocalizedString("Share your photos via Facebook, email, Twitter, or instant message.", comment: ""))
        pages = [page1, page2, page3]
        
        let views: [String: UIView] = ["view": view, "page1": page1.view, "page2": page2.view, "page3": page3.view]
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[page1(==view)]|", options: [], metrics: nil, views: views)
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[page1(==view)][page2(==view)][page3(==view)]|", options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views)
        NSLayoutConstraint.activate(verticalConstraints + horizontalConstraints)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    private func createAndAddTutorialStep(_ backgroundImageName: String, iconImageName: String, text: String) -> TutorialStepViewController {
        let tutorialStep = storyboard!.instantiateViewController(withIdentifier: "TutorialStepViewController") as! TutorialStepViewController
        tutorialStep.view.translatesAutoresizingMaskIntoConstraints = false
        tutorialStep.backgroundImage = UIImage(named: backgroundImageName)
        tutorialStep.iconImage = UIImage(named: iconImageName)
        tutorialStep.text = text
        
        scrollView.addSubview(tutorialStep.view)
        
        addChild(tutorialStep)
        tutorialStep.didMove(toParent: self)
        
        return tutorialStep
    }
}

extension TutorialViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
        scrollView.contentOffset.y = 0
        let pageWidth = scrollView.bounds.width
        let pageFraction = scrollView.contentOffset.x / pageWidth
        pageControl.currentPage = Int(round(pageFraction))
        print(Float(pageFraction))
        if Float(pageFraction) > 1.0 {
              NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        }
        if Float(pageFraction) > 2.0 {
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(navVC, animated: true)


        }
    }
    
    
    @IBAction func skipeSteps(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(navVC, animated: true)
        
    }
    
}
