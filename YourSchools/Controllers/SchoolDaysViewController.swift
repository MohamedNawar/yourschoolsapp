//
//  SchoolDaysViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import LanguageManager_iOS

class SchoolDaysViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var tableView: UITableView!
    var tag = Int()
    var allAvilableExams = Exams(){
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if tag != 0 {
        showAllAvilableExams()
        }
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tag == 1{
            return allAvilableExams.data?.count ?? 0
        }else{
        return 7
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDaysCell", for: indexPath)
            let title = cell.viewWithTag(1) as! UILabel
        if tag == 1{
            title.text = allAvilableExams.data?[indexPath.row].name ?? ""
        }else{
        switch indexPath.row {
        case 0:
        
                title.text = NSLocalizedString("Saturday", comment:"السبت")
                 title.textColor = #colorLiteral(red: 0, green: 0.3285208941, blue: 0.5748849511, alpha: 0.6665239726)
            
        case 1:
         
                title.text = NSLocalizedString("Sunday", comment:"الأحد")
                 title.textColor = #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
            
        case 2:
                title.text = NSLocalizedString("Monday", comment:"الأثنين")
                 title.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            
        case 3:
          
                title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                 title.textColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
            
        case 4:
         
                title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                 title.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            
        case 5:
           
                title.text = NSLocalizedString("Thursday", comment:"الخميس")
                 title.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
            
        case 6:
                title.text = NSLocalizedString("Friday", comment:"الجمعة")
                 title.textColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)

        default:
            title.text = ""
        }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tag == 1{
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ExamScheduleViewController") as! ExamScheduleViewController
            navVC.title = NSLocalizedString("Exams Table", comment: "")
            navVC.id = allAvilableExams.data?[indexPath.row].id ?? 0; self.navigationController?.pushViewController(navVC, animated: true)

        }else{
        let navVC = storyboard?.instantiateViewController(withIdentifier: "CurriculumViewController") as! CurriculumViewController
            navVC.title = NSLocalizedString("School Schedule", comment: "")
        switch indexPath.row {
        case 0:
            navVC.day = 6
        case 1:
            navVC.day = 7
        case 2:
            navVC.day = 1
        case 3:
            navVC.day = 2
        case 4:
            navVC.day = 3
        case 5:
            navVC.day = 4
        case 6:
            navVC.day = 5
        default: 0
        }
        self.navigationController?.pushViewController(navVC, animated: true)
        }
    }
    private func showAllAvilableExams(){
        let header = APIs.Instance.getHeader()
        print(header)
        print(APIs.Instance.showAllAvilableExams())
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showAllAvilableExams() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.allAvilableExams = try JSONDecoder().decode(Exams.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }

}
