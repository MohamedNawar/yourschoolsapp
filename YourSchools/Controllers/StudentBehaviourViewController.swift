//
//  StudentBehaviourViewController.swift
//  YourSchools
//
//  Created by iMac on 11/5/18.
//  Copyright © 2018 iMac. All rights reserved.
//


import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import HCSStarRatingView
import LanguageManager_iOS
class StudentBehaviourViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    var currentUser = CurrentUser(){
        didSet{
            tableView.reloadData()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        showstudent(id:userData.Instance.data?.id ?? 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = currentUser.data?.warnings?.count ?? 0
        return count+2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StudentBehaviourHeader", for: indexPath)
            let image = cell.viewWithTag(120) as! UIImageView
            let lbl1 = cell.viewWithTag(1) as! UILabel
            let rate = cell.viewWithTag(2) as! HCSStarRatingView
            let btn3 = cell.viewWithTag(3) as! UIButton
            let btn4 = cell.viewWithTag(4) as! UIButton
            if let data = currentUser.data {
                lbl1.text = data.name ?? ""
                rate.value = CGFloat(data.rate_student ?? 0)
                btn4.setTitle(data.section?.name ?? "", for: .normal)
                btn3.setTitle(data.preference_student ?? "", for: .normal)
                image.sd_setImage(with: URL(string: self.currentUser.data?.avatar ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))
            }
            return cell
        }
        if indexPath.row == 1 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "AboutStudentBehaviourCell", for: indexPath)
            let lbl6 = cell1.viewWithTag(6) as! UILabel
            if let data = currentUser.data {
                lbl6.text = data.about ?? ""
            }
            return cell1
        }else{
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "StudentBehaviourCell", for: indexPath)
        let lbl7 = cell2.viewWithTag(7) as! UILabel
        if let data = currentUser.data {
            if LanguageManager.shared.currentLanguage.rawValue == "ar" {
            lbl7.text = "\(data.warnings?[indexPath.row-2].reason ?? "")   \(data.warnings?[indexPath.row-2].date ?? "")  \(NSLocalizedString("Date", comment: ""))"
            }else{
                lbl7.text = "\(data.warnings?[indexPath.row-2].reason ?? "")  \(NSLocalizedString("Date", comment: "")) \(data.warnings?[indexPath.row-2].date ?? "")"
            }
        }
        return cell2
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   private func showstudent(id:Int){
        let header = APIs.Instance.getHeader()
    let par = [
        "remove-notifications" : true
    ] as! [String:Any]
    print(par)
        HUD.show(.progress, onView: self.view)
    print(APIs.Instance.showCurrentStudent(id: id))
        Alamofire.request(APIs.Instance.showCurrentStudent(id: id) , method: .get, parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.currentUser = try JSONDecoder().decode(CurrentUser.self, from: response.data!)

                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
