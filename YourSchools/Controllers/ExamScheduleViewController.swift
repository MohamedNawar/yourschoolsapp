//
//  ExamScheduleViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import LanguageManager_iOS

class ExamScheduleViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var tableView: UITableView!
    var currentExamDetails = Exam(){
        didSet{
            tableView.reloadData()
        }
    }
    var id = Int(){
        didSet{
          showCurrentExamDetails(id:id)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentExamDetails.data?.subjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExamScheduleCell", for: indexPath)
        let lbl1 = cell.viewWithTag(1) as! UILabel
        let lbl2 = cell.viewWithTag(2) as! UILabel
        let lbl3 = cell.viewWithTag(3) as! UILabel
        let lbl4 = cell.viewWithTag(4) as! UILabel
        if let currntExamData = currentExamDetails.data?.subjects?[indexPath.row] {
            lbl1.text = currntExamData.date ?? ""
            lbl2.text = currntExamData.subject?.name ?? ""
            lbl3.text = currntExamData.day ?? ""
            lbl4.text = currntExamData.time_formated ?? ""
        }
        if LanguageManager.shared.currentLanguage == .ar {
            lbl2.textAlignment = .right
            lbl3.textAlignment = .right
            lbl4.textAlignment = .right
        }
        return cell
    }
    private func showCurrentExamDetails(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showCurrentExams(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.currentExamDetails = try JSONDecoder().decode(Exam.self, from: response.data!)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
   
}
