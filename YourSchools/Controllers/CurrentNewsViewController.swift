//
//  CurrentNewsViewController.swift
//  YourSchools
//
//  Created by iMac on 11/5/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import SDWebImage
import LanguageManager_iOS
import PKHUD
import Alamofire
import FCAlertView
import ImageSlideshow
import AlamofireImage


class CurrentNewsViewController: CustomBaseVC {
    
    
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsIsUrgent: UIButton!
    @IBOutlet weak var viewCount: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsBodyContent: UILabel!
    @IBOutlet weak var shareLbl: UILabel!
    var currentNews = NewsData()
    var sliderImages: [AlamofireSource]?
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSchoolNew(id: "\(self.currentNews.id ?? 0)")
        
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.lightGray
        pageIndicator.pageIndicatorTintColor = UIColor.darkGray
        imageSlider.pageIndicator = pageIndicator
        
        sliderImages = [AlamofireSource]()
        if (currentNews.album?.isEmpty ?? Bool()) {
            guard let imageSource = AlamofireSource(urlString: currentNews.avatar ?? "") else {return}
                                 sliderImages?.append(imageSource)
      
        }else{
          currentNews.album?.forEach() {
                    if let imageSource = AlamofireSource(urlString: $0.link ?? ""){
                        sliderImages?.append(imageSource)
                    }
                }
        }
        imageSlider.setImageInputs(sliderImages!)
        
        newsTitle.text = currentNews.title ?? ""
        if currentNews.is_urgent == 1 {
            newsIsUrgent.isHidden = false
        }else{
            newsIsUrgent.isHidden = true
        }
        viewCount.text = "\(currentNews.views_count ?? 0)"
        newsBodyContent.text = currentNews.body
        newsDate.text = currentNews.created_at ?? ""
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if LanguageManager.shared.currentLanguage == .ar {
            newsBodyContent.textAlignment = .right
            viewCount.textAlignment = .right
            newsDate.textAlignment = .right
            shareLbl.textAlignment = .right

        }
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    @IBAction func share(_ sender: Any) {
            let myWebsite = NSURL(string: self.currentNews.body ?? "")
            let shareAll = [ myWebsite]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
      
    }
    private func loadSchoolNew(id:String){
          let header = APIs.Instance.getHeader()
          HUD.show(.progress, onView: self.view)
      Alamofire.request(APIs.Instance.getSchoolNew(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
              switch(response.result) {
              case .success(let value):
                  print(value)
                  HUD.hide()
                  print(value)
                  let temp = response.response?.statusCode ?? 400
                  if temp >= 300 {
                      
                      do {
                          let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                          self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                          print(err.errors)
                      }catch{
                          print("errorrrrelse")
                      }
                  }else{
                      do {
                          self.currentNews = try JSONDecoder().decode(NewsData.self, from: response.data!)
                      }catch{
                          print(error)
                          HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
                      }
                  }
              case .failure(_):
                  HUD.hide()
                  let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                  HUD.flash(.label(lockString), delay: 1.0)
                  break
              }
          }
      }
      func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
          let alert = FCAlertView()
          alert.avoidCustomImageTint = true
          let updatedFrame = alert.bounds
          alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
          alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
      }
}
