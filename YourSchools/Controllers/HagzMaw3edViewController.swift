//
//  HagzMaw3edViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/27/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

import AVFoundation
import Alamofire
import PKHUD
class HagzMaw3edViewController: UIViewController ,AVAudioRecorderDelegate, AVAudioPlayerDelegate  {
    var audioPlayer : AVAudioPlayer!
    var recordingSession:AVAudioSession!
    var audioRecorder:AVAudioRecorder!
    var settings = [String : Int]()
    var totalTime = 0
    var didRecord = false
    var countdownTimer: Timer!
    
    @IBAction func playPressed(_ sender: Any) {
        if !audioRecorder.isRecording {
            self.audioPlayer = try! AVAudioPlayer(contentsOf: audioRecorder.url)
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.delegate = self
            audioPlayer.volume = 1
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
            } catch _ {
            }
            self.audioPlayer.play()
        }
    }
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var recordingLbl: UILabel!
    @IBOutlet var durationLbl: UILabel!
    @IBAction func startRecording(_ sender: Any) {
        UIImpactFeedbackGenerator().impactOccurred()
        self.startRecording()
        recordingLbl.isHidden = false
        playBtn.isHidden = true
        startTimer()
    }
    @IBAction func stopRecording(_ sender: Any) {
        self.finishRecording(success: true)
        UINotificationFeedbackGenerator().notificationOccurred(.success)
        recordingLbl.isHidden = true
        playBtn.isHidden = false
        endTimer()
        didRecord = true
        
    }
    
    @IBOutlet var detailsField: UITextView!
    @IBOutlet var addressField: DesignableTextField!
    @IBOutlet weak var caseType: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        askForRecordingPremission()
    }

    func askForRecordingPremission(){
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        print(soundURL)
        return soundURL as NSURL?
    }
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    func endTimer() {
        countdownTimer.invalidate()
        totalTime = 0
    }
    @objc func updateTime() {
        totalTime += 1
        durationLbl.text = "\(timeFormatted(totalTime))"
        
    }
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }

}
