//
//  AnnualInstallmentsViewController.swift
//  YourSchools
//
//  Created by iMac on 11/5/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

class AnnualInstallmentsViewController:CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var tableView: UITableView!
    let imageArray = [#imageLiteral(resourceName: "img08"),#imageLiteral(resourceName: "img09"),#imageLiteral(resourceName: "img10")]
    var annualInstallments = Installments(){
        didSet{
            if annualInstallmentsArray.count > 0 {
                annualInstallmentsArray.removeAll()
            }
            if let paid = annualInstallments.paid {
                annualInstallmentsArray.append(paid)
            }
            if let not_paid = annualInstallments.not_paid {
                annualInstallmentsArray.append(not_paid)
            }
            if let partially_paid = annualInstallments.partially_paid {
                annualInstallmentsArray.append(partially_paid)
            }
             tableView.reloadData()
        }
    }
    @IBOutlet weak var noDataAvilableView: UIView!
    var annualInstallmentsArray = [Paid]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadAnnualInstallmentsData();
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = annualInstallmentsArray[section].data?.count ?? 0
        return count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnnualInstallmentsHeader", for: indexPath)
            let lbl7 = cell.viewWithTag(7) as! UILabel
            lbl7.text = annualInstallmentsArray[indexPath.section].group
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnualInstallmentsCell", for: indexPath)
        let lbl1 = cell.viewWithTag(1) as! UILabel
        let lbl2 = cell.viewWithTag(2) as! UILabel
        let image = cell.viewWithTag(5) as! UIImageView
        image.image = imageArray[indexPath.section]
        let status = annualInstallmentsArray[indexPath.section].status ?? ""
        let price = annualInstallmentsArray[indexPath.section].data?[indexPath.row-1].price ?? 0
        lbl1.text = annualInstallmentsArray[indexPath.section].data?[indexPath.row-1].date
        lbl2.text = "The amount: \(price) \(status)"
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return annualInstallmentsArray.count ?? 0
    }
    func loadAnnualInstallmentsData(){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.getSchoolInstallments() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    
                    do {
                        self.annualInstallments = try JSONDecoder().decode(Installments.self, from: response.data!)
                        if self.annualInstallments.not_paid?.data?.count ?? 0 == 0 && self.annualInstallments.paid?.data?.count ?? 0 == 0 && self.annualInstallments.partially_paid?.data?.count ?? 0 == 0 {
                            self.noDataAvilableView.isHidden = false
                        }else{
                            self.noDataAvilableView.isHidden = true
                        }
                    }catch{
                        print(error)
                    }
                }
            case .failure(_):
                HUD.hide()
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
