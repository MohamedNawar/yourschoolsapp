//
//  ViewController.swift
//  YourSchools
//
//  Created by iMac on 12/24/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import FCAlertView
import PKHUD
class ShowImageCollectionViewController:CustomBaseVC , UICollectionViewDataSource , UICollectionViewDelegate  {
    var schoolMedia = SchoolMedia(){
        didSet{
            CategoryCollectionView.reloadData()
        }
    }
    var albumImages = [Image]()
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let layout = CategoryCollectionView?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
//        getSchoolMedia(id: id)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albumImages.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath)
        let image = cell.viewWithTag(1) as! UIImageView
        image.sd_setImage(with: URL(string: albumImages[indexPath.row].url ?? ""), placeholderImage: #imageLiteral(resourceName: "img34"))

        
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var sectionHeaderView = UICollectionReusableView()
        if collectionView == self.CategoryCollectionView {
            switch kind {
                
            case UICollectionView.elementKindSectionHeader:
                sectionHeaderView = CategoryCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MainSectionsHeader", for: indexPath)
                
                return sectionHeaderView
                collectionView.reloadData()
                
            default:
                assert(false, "Unexpected element kind")
            }
            
        }
        
        
        return sectionHeaderView
        
    }
    
//    private func getSchoolMedia(id:Int){
//        let header = APIs.Instance.getHeader()
//        HUD.show(.progress, onView: self.view)
//        print(APIs.Instance.getSchoolMedia(id:id))
//    Alamofire.request(APIs.Instance.getSchoolMedia() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error".localized(), SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "Untitled-11"))
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.schoolMedia = try JSONDecoder().decode(SchoolMedia.self, from: response.data!)
//                    }catch{
//                        print(error)
//                        HUD.flash(.label("Error Try Again".localized()), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                HUD.flash(.label(lockString), delay: 1.0)
//                break
//            }
//        }
//    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
extension ShowImageCollectionViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

extension ShowImageCollectionViewController: PinterestLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {

        return CGFloat(200)
//return photos[indexPath.item].size.height
    }
    

}


